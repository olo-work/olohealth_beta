import 'dart:async';
import 'package:olohealth/app/home/models/entry.dart';
import 'package:olohealth/app/home/models/konsultasi.dart';
import 'package:olohealth/app/home/models/pasien.dart';
import 'package:olohealth/app/home/models/profile.dart';
import 'package:olohealth/app/home/models/resep.dart';
import 'package:olohealth/services/auth.dart';
import 'package:meta/meta.dart';
import 'package:olohealth/app/home/models/vitalsign.dart';

import 'api_path.dart';
import 'firestore_service.dart';

abstract class Database {
  Future<void> createProfile(Profile profile);
  Future<void> deleteProfile(Profile profile);
  Future<void> deleteUser(User user);
  Stream<List<Profile>> readSlave();
  Stream<List<User>> readUsers();
  Stream<User> readUser({String userId});
  Future<void> setPasien(Pasien pasien);
   Future<void> setPasien2(Pasien pasien);
  Stream<List<Pasien>> readPasien();
  Stream<List<Profile>> readTest();
  Future<void> setTest(Profile profile);
  Future<void> deletePasien(Pasien pasien);
  Future<void> setMedical(Medical medical);
  Future<void> deleteMedical(Medical medical);
  Stream<List<Medical>> medicalStream({Pasien pasien});
  Stream<List<Medical>> medicalStream2({User user});
  Stream<List<Profile>> testStream({User user});
  Future<void> setMedical2(Medical medical);
  Stream<List<Pasien>> streamPasien({String doctor, int date});
  Stream<List<Resep>> resepStream({Pasien pasien});
  Future<void> setResep(Resep resep);
  Future<void> deleteResep(Resep resep);
  Stream<List<Pasien>> queryPasien({int date});
  Future<void> deleteVital(Vital vital);
  Stream<List<Vital>> vitalStream({String pasienId});
  Future<void> setVital(Vital vital);
  Stream<Vital> readVital({String userId});
  Future<void> updatePasien(Pasien pasien);
  Future<void> updateVital(Vital vital);
  Stream<List<User>> usersStream({String role});
  Future<void> setKonsultasi(Konsultasi consult);
  Stream<List<Konsultasi>> queryConsult({int date});
}

String documentIdFromCurrentDate() => DateTime.now().toIso8601String();

class FirestoreDatabase implements Database {
  FirestoreDatabase({@required this.uid}) : assert(uid != null);
  final String uid;

  final _service = FirestoreService.instance;
  @override
  Future<void> createProfile(Profile profile) async => await _service.setData(
        path: APIPath.profile(uid, profile.id),
        data: profile.toMap(),
      );

  @override
  Future<void> deleteProfile(Profile profile) async =>
      await _service.deleteData(
        path: APIPath.profile(uid, profile.id),
      );

  @override
  Future<void> deleteUser(User user) async => await _service.deleteData(
        path: APIPath.user(uid),
      );

  @override
  Stream<List<Profile>> readSlave() => _service.collectionStream(
        path: APIPath.profiles(uid),
        builder: (data, documentId) => Profile.fromMap(data, documentId),
      );

  @override
  Stream<List<Profile>> readTest() => _service.collectionStream(
        path: APIPath.tests(),
        builder: (data, documentId) => Profile.fromMap(data, documentId),
      );

  @override
  Stream<List<User>> readUsers() => _service.collectionStream(
        path: APIPath.users2(),
        builder: (data, documentId) => User.fromMap(data, documentId),
      );

  @override
  Stream<User> readUser({String userId}) => _service.documentStream(
        path: APIPath.user2(userId),
        builder: (data, documentId) => User.fromMap(data, documentId),
      );

  Stream<Vital> readVital({String userId}) => _service.documentStream(
        path: APIPath.vital(userId),
        builder: (data, documentId) => Vital.fromMap(data, documentId),
      );

  @override
  Stream<List<Pasien>> readPasien() => _service.collectionStream(
        path: APIPath.users(),
        builder: (data, documentId) => Pasien.fromMap(data, documentId),
      );

  @override
  Future<void> setPasien(Pasien pasien) async => await _service.setData(
        path: APIPath.user(pasien.id),
        data: pasien.toMap(),
      );

  @override
  Future<void> updatePasien(Pasien pasien) async => await _service.updateData(
        path: APIPath.user(pasien.id),
        data: pasien.toMap2(),
      );

   @override
  Future<void> setPasien2(Pasien pasien) async => await _service.setData(
        path: APIPath.user(pasien.id),
        data: pasien.toMap2(),
      );

  @override
  Future<void> deletePasien(Pasien pasien) async => await _service.deleteData(
        path: APIPath.user(uid),
      );

  @override
  Future<void> setTest(Profile profile) async => await _service.setData(
        path: APIPath.test(profile.id),
        data: profile.toMap(),
      );

  @override
  Future<void> setMedical2(Medical medical) async => await _service.setData(
        path: APIPath.medical2(medical.id),
        data: medical.toMap(),
      );

  @override
  Future<void> setMedical(Medical medical) async => await _service.setData(
        path: APIPath.medical(uid, medical.id),
        data: medical.toMap(),
      );

  @override
  Future<void> deleteMedical(Medical medical) async =>
      await _service.deleteData(path: APIPath.medical(uid, medical.id));

  @override
  Future<void> deleteVital(Vital vital) async =>
      await _service.deleteData(path: APIPath.vital(vital.id));

  @override
  Stream<List<Vital>> vitalStream({String pasienId}) =>
      _service.collectionStream<Vital>(
        path: APIPath.vitals(),
        queryBuilder: (query) => query.where('pasienId', isEqualTo: pasienId).where('berat', isGreaterThanOrEqualTo: 0 ),
        builder: (data, documentID) => Vital.fromMap(data, documentID),
        // sort: (lhs, rhs) => rhs.lastCheckUp.compareTo(lhs.lastCheckUp),
      );

  @override
  Stream<List<Medical>> medicalStream({Pasien pasien}) =>
      _service.collectionStream<Medical>(
        path: APIPath.medicals2(),
        queryBuilder: pasien != null
            ? (query) => query.orderBy('lastCheckUp', descending: true).where('uid', isEqualTo: pasien.id)
            : null,
        builder: (data, documentID) => Medical.fromMap(data, documentID),
        // sort: (lhs, rhs) => rhs.lastCheckUp.compareTo(lhs.lastCheckUp),
      );

  @override
  Stream<List<Medical>> medicalStream2({User user}) =>
      _service.collectionStream<Medical>(
        path: APIPath.medicals2(),
        queryBuilder: user != null
            ? (query) => query.where('uid', isEqualTo: user.uid)
            : null,
        builder: (data, documentID) => Medical.fromMap(data, documentID),
        // sort: (lhs, rhs) => rhs.lastCheckUp.compareTo(lhs.lastCheckUp),
      );

  @override
  Stream<List<Profile>> testStream({User user}) =>
      _service.collectionStream<Profile>(
        path: APIPath.tests(),
        queryBuilder: user != null
            ? (query) => query.where('uid', isEqualTo: user.uid)
            : null,
        builder: (data, documentID) => Profile.fromMap(data, documentID),
        // sort: (lhs, rhs) => rhs.lastCheckUp.compareTo(lhs.lastCheckUp),
      );
  @override
  Stream<List<Pasien>> streamPasien({String doctor, int date}) =>
      _service.collectionStream<Pasien>(
        path: APIPath.users(),
        queryBuilder: (query) => query
            .where('withDoc', isEqualTo: doctor)
            .where('queryDate', isGreaterThanOrEqualTo: date),
        builder: (data, documentID) => Pasien.fromMap(data, documentID),
        // sort: (lhs, rhs) => rhs.lastCheckUp.compareTo(lhs.lastCheckUp),
      );

  @override
  Stream<List<Pasien>> queryPasien({int date}) =>
      _service.collectionStream<Pasien>(
        path: APIPath.users(),
        queryBuilder: (query) => query
            .where('queryDate', isGreaterThanOrEqualTo: date),
        builder: (data, documentID) => Pasien.fromMap(data, documentID),
        // sort: (lhs, rhs) => rhs.lastCheckUp.compareTo(lhs.lastCheckUp),
      );

  @override
  Stream<List<Resep>> resepStream({Pasien pasien}) =>
      _service.collectionStream<Resep>(
        path: APIPath.reseps(),
        queryBuilder: pasien != null
            ? (query) => query.orderBy('resepDate', descending: true).where('pasienId', isEqualTo: pasien.id)
            : null,
        builder: (data, documentID) => Resep.fromMap(data, documentID),
        // sort: (lhs, rhs) => rhs.lastCheckUp.compareTo(lhs.lastCheckUp),
      );

  @override
  Future<void> setResep(Resep resep) async => await _service.setData(
        path: APIPath.resep(resep.id),
        data: resep.toMap(),
      );

  @override
  Future<void> deleteResep(Resep resep) async => await _service.deleteData(
        path: APIPath.resep(resep.id),
      );

  @override
  Future<void> setVital(Vital vital) async => await _service.setData(
        path: APIPath.vital(vital.id),
        data: vital.toMap(),
      );

  @override
  Future<void> updateVital(Vital vital) async => await _service.setData(
        path: APIPath.vital(vital.id),
        data: vital.updateMap(),
      );

  @override
  Stream<List<User>> usersStream({String role}) =>
      _service.collectionStream<User>(
        path: APIPath.users2(),
        queryBuilder: (query) => query.where('role', isEqualTo: role),
        builder: (data, documentID) => User.fromMap(data, documentID),
        // sort: (lhs, rhs) => rhs.lastCheckUp.compareTo(lhs.lastCheckUp),
      );

  @override
  Future<void> setKonsultasi(Konsultasi consult) async => await _service.setData(
        path: APIPath.konsultasi(consult.id),
        data: consult.toMap(),
      );

  @override
  Stream<List<Konsultasi>> queryConsult({int date}) =>
      _service.collectionStream<Konsultasi>(
        path: APIPath.konsultasis(),
        queryBuilder: (query) => query
            .where('tanggal', isGreaterThanOrEqualTo: date),
        builder: (data, documentID) => Konsultasi.fromMap(data, documentID),
        // sort: (lhs, rhs) => rhs.lastCheckUp.compareTo(lhs.lastCheckUp),
      );
}
