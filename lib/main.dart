import 'dart:html';
import 'package:olohealth/UiFake.dart' if (dart.library.html) 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'app/landing_page.dart';
import 'services/auth.dart';
import 'package:flutter/services.dart';

void main() {
  ui.platformViewRegistry.registerViewFactory(
      'test',
      (int viewId) => IFrameElement()
        ..width = '640'
        ..height = '360'
        ..src = 'https://www.youtube.com/embed/IyFZznAk69U'
        ..style.border = 'none');
  // RenderErrorBox.backgroundColor = Colors.transparent;
  // RenderErrorBox.textStyle = ui.TextStyle(color: Colors.transparent);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Set landscape orientation
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);
    return Provider<AuthBase>(
      create: (context) => Auth(),
      child: MaterialApp(
        title: 'OLO HEALTH',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: LandingPage(),
        // home: Scaffold(
        //   body: Text('test'),
        // ),
      ),
    );
  }
}
