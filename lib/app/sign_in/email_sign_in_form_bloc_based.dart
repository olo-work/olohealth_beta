// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:provider/provider.dart';
// import 'package:sexslave/app/sign_in/email_sign_in_bloc.dart';
// import 'package:sexslave/app/sign_in/email_sign_in_model.dart';
// import 'package:sexslave/common_widgets/form_submit_button.dart';
// import 'package:sexslave/common_widgets/platform_exeption_alert_dialog.dart';
// import 'package:sexslave/services/auth.dart';

// class EmailSignInFormBlocBased extends StatefulWidget {
//   EmailSignInFormBlocBased({@required this.bloc});
//   final EmailSignInBloc bloc;

//   static Widget create(BuildContext context) {
//     final AuthBase auth = Provider.of<AuthBase>(context);
//     return Provider<EmailSignInBloc>(
//       create: (context) => EmailSignInBloc(auth: auth),
//       child: Consumer<EmailSignInBloc>(
//           builder: (context, bloc, _) => EmailSignInFormBlocBased(bloc: bloc)),
//       dispose: (context, bloc) => bloc.dispose(),
//     );
//   }

//   @override
//   _EmailSignInFormState createState() => _EmailSignInFormState();
// }

// class _EmailSignInFormState extends State<EmailSignInFormBlocBased> {
//   final TextEditingController _emailController = TextEditingController();
//   final TextEditingController _passwordController = TextEditingController();
//   final FocusNode _emailFocusNode = FocusNode();
//   final FocusNode _passwordFocusNode = FocusNode();

//   @override
//   void dispose() {
//     _emailController.dispose();
//     _passwordController.dispose();
//     _emailFocusNode.dispose();
//     _passwordFocusNode.dispose();
//     super.dispose();
//   }

//   Future<void> _submit() async {
//     try {
//       await widget.bloc.submit();
//       Navigator.of(context).pop();
//     } on PlatformException catch (e) {
//       PlatformExeptionAlertDialog(
//         title: 'Sign in Fail!',
//         exception: e,
//       ).show(context);
//     }
//   }

//   void _emailEditingCompleted(EmailSignInModel model) {
//     final newFocus = model.emaolValidator.isValid(model.email)
//         ? _passwordFocusNode
//         : _emailFocusNode;
//     FocusScope.of(context).requestFocus(newFocus);
//   }

//   void _toggleForm() {
//     widget.bloc.toggleFormType();
//     _emailController.clear();
//     _passwordController.clear();
//   }

//   List<Widget> _buildChildren(EmailSignInModel model) {
//     return [
//       _buildEmailText(model),
//       SizedBox(height: 8.0),
//       _buildPasswordText(model),
//       SizedBox(height: 8.0),
//       FormSubmitButton(
//         text: model.primaryButtonText,
//         onPressed: model.canSubmit ? _submit : null,
//       ),
//       SizedBox(height: 8.0),
//       FlatButton(
//         child: Text(model.secondaryButtonText),
//         onPressed: !model.isLoading ? _toggleForm : null,
//       ),
//     ];
//   }

//   TextField _buildPasswordText(EmailSignInModel model) {
//     return TextField(
//       controller: _passwordController,
//       focusNode: _passwordFocusNode,
//       decoration: InputDecoration(
//         labelText: 'Password',
//         errorText: model.passwordErrorText,
//         enabled: model.isLoading == false,
//       ),
//       obscureText: true,
//       textInputAction: TextInputAction.done,
//       onChanged: widget.bloc.updatePassword,
//       onEditingComplete: _submit,
//     );
//   }

//   TextField _buildEmailText(EmailSignInModel model) {
//     return TextField(
//       controller: _emailController,
//       focusNode: _emailFocusNode,
//       decoration: InputDecoration(
//         labelText: 'Email',
//         hintText: 'your@email.com',
//         errorText: model.emailErrorText,
//         enabled: model.isLoading == false,
//       ),
//       autocorrect: false,
//       keyboardType: TextInputType.emailAddress,
//       textInputAction: TextInputAction.next,
//       onChanged: widget.bloc.updateEmail,
//       onEditingComplete: () => _emailEditingCompleted(model),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return StreamBuilder<EmailSignInModel>(
//         stream: widget.bloc.modelStream,
//         initialData: EmailSignInModel(),
//         builder: (context, snapshot) {
//           final EmailSignInModel model = snapshot.data;
//           return Padding(
//             padding: const EdgeInsets.all(16.0),
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.stretch,
//               mainAxisSize: MainAxisSize.min,
//               children: _buildChildren(model),
//             ),
//           );
//         });
//   }
// }
