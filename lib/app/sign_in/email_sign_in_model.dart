import 'validator.dart';

enum EmailSignInFormType { signIn, register, reset }

class EmailSignInModel with EmailAndPasswordValidator {
  EmailSignInModel({
    this.email = '',
    this.password = '',
    this.formType = EmailSignInFormType.signIn,
    this.isLoading = false,
    this.submitted = false,
  });

  final String email;
  final String password;
  final EmailSignInFormType formType;
  final bool isLoading;
  final bool submitted;

  String get primaryButtonText {
    return formType == EmailSignInFormType.signIn
        ? 'SignIn'
        : 'Reset';
  }

  String get secondaryButtonText {
    return formType == EmailSignInFormType.signIn
        ? 'Reset Your Password'
        : 'Have an account? Sign in';
  }

  bool get canSubmit {
    return emaolValidator.isValid(email) &&
        passwordValidator.isValid(password) &&
        !isLoading;
  }

  String get passwordErrorText {
    bool showErrorText = submitted && !passwordValidator.isValid(password);
    return showErrorText ? passwordEroorNotValid : null; 
  }

  String get emailErrorText {
    bool showErrorText = submitted && !emaolValidator.isValid(email);
    return showErrorText ? emailEroorNotValid : null;
  }

  EmailSignInModel copyWith({
    String email,
    String password,
    EmailSignInFormType formType,
    bool isLoading,
    bool submitted,
  }) {
    return EmailSignInModel(
      email: email ?? this.email,
      password: password ?? this.password,
      formType: formType ?? this.formType,
      isLoading: isLoading ?? this.isLoading,
      submitted: submitted ?? this.submitted,
    );
  }
}
