import 'dart:async';
import 'package:flutter/foundation.dart';

import '../../services/auth.dart';

class SignInBloc {
  SignInBloc({@required this.auth, @required this.isLoading,});
  final AuthBase auth;
  final ValueNotifier isLoading;

  Future<User> _signIn(Future<User> Function() signInMethod) async {
    try {
      isLoading.value = true;
      return await signInMethod();
    } catch (e) {
      isLoading.value = false;
      rethrow;
    }
  }

  Future<User> signInAnonymously() async => await _signIn(auth.signInAnonymously);
  Future<User> signInGoogle()async => await _signIn(auth.signInGoogle);
  Future<User> signInFacebook()async => await _signIn(auth.signInFacebook);
}
