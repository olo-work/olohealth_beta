abstract class StringValidator {
  bool isValid(String value);
}

class NonEmptyValidator implements StringValidator {
  @override
  bool isValid(String value) {
    return value.isNotEmpty;
  }
}

class EmailAndPasswordValidator {
  final StringValidator emaolValidator = NonEmptyValidator();
  final StringValidator passwordValidator = NonEmptyValidator();
  final String emailEroorNotValid = 'Email can\' be Empty';
  final String passwordEroorNotValid = 'Password can\' be Empty';
}
