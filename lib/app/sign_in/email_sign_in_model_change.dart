import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../services/auth.dart';
import 'email_sign_in_model.dart';
import 'validator.dart';

class EmailSignInChangeModel with EmailAndPasswordValidator, ChangeNotifier {
  EmailSignInChangeModel({
    @required this.auth,
    this.email = '',
    this.password = '',
    this.formType = EmailSignInFormType.signIn,
    this.isLoading = false,
    this.submitted = false,
    this.reset = false,
  });

  final AuthBase auth;
  String email;
  String password;
  EmailSignInFormType formType;
  bool isLoading;
  bool submitted;
  bool reset;

  Future<void> submit() async {
    updateWith(
      submitted: true,
      isLoading: true,
    );
    try {
      if (formType == EmailSignInFormType.signIn) {
        await auth.signinEmail(
          email,
          password,
        );
      } else {
        await auth.resetPassword(
          email,
        );
        print('password reset!!!');
      }
    } catch (e) {
      updateWith(isLoading: false);
      rethrow;
    }
  }

  String get primaryButtonText {
    return formType == EmailSignInFormType.signIn ? 'SignIn' : 'Reset';
  }

  String get secondaryButtonText {
    return formType == EmailSignInFormType.signIn
        ? 'Reset Password? click here'
        : 'Have an account? Sign in';
  }

  String get dialogText {
    return formType == EmailSignInFormType.signIn
        ? 'Sign in to app'
        : 'please cek your email for reset password';
  }

  bool get canSubmit {
    if (formType == EmailSignInFormType.signIn) {
      return emaolValidator.isValid(email) &&
          passwordValidator.isValid(password) &&
          !isLoading;
    }
    return emaolValidator.isValid(email) && !isLoading;
  }

  String get passwordErrorText {
    bool showErrorText = submitted && !passwordValidator.isValid(password);
    return showErrorText ? passwordEroorNotValid : null;
  }

  String get emailErrorText {
    bool showErrorText = submitted && !emaolValidator.isValid(email);
    return showErrorText ? emailEroorNotValid : null;
  }

  void toggleFormType() {
    final formType = this.formType == EmailSignInFormType.signIn
        ? EmailSignInFormType.reset
        : EmailSignInFormType.signIn;
    updateWith(
      email: '',
      password: '',
      formType: formType,
      submitted: false,
      isLoading: false,
      reset: !reset,
    );
  }

  void updateEmail(String email) => updateWith(email: email);
  void updatePassword(String password) => updateWith(password: password);

  void updateWith({
    String email,
    String password,
    EmailSignInFormType formType,
    bool isLoading,
    bool submitted,
    bool reset,
  }) {
    this.email = email ?? this.email;
    this.password = password ?? this.password;
    this.formType = formType ?? this.formType;
    this.isLoading = isLoading ?? this.isLoading;
    this.submitted = submitted ?? this.submitted;
    this.reset = reset ?? this.reset;
    notifyListeners();
  }
}
