// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:provider/provider.dart';
// import 'package:sexslave/app/sign_in/email_sign_in_model.dart';
// import 'package:sexslave/app/sign_in/validator.dart';
// import 'package:sexslave/common_widgets/form_submit_button.dart';
// import 'package:sexslave/common_widgets/platform_exeption_alert_dialog.dart';
// import 'package:sexslave/services/auth.dart';

// class EmailSignInFormStateful extends StatefulWidget with EmailAndPasswordValidator {
//   @override
//   _EmailSignInFormState createState() => _EmailSignInFormState();
// }

// class _EmailSignInFormState extends State<EmailSignInFormStateful> {
//   final TextEditingController _emailController = TextEditingController();
//   final TextEditingController _passwordController = TextEditingController();
//   final FocusNode _emailFocusNode = FocusNode();
//   final FocusNode _passwordFocusNode = FocusNode();

//   String get _email => _emailController.text;
//   String get _password => _passwordController.text;
//   EmailSignInFormType _formType = EmailSignInFormType.signIn;
//   bool _submitted = false;
//   bool _isLoading = false;

//   @override
//   void dispose() {
//     _emailController.dispose();
//     _passwordController.dispose();
//     _emailFocusNode.dispose();
//     _passwordFocusNode.dispose();
//     super.dispose();
//   }

//   Future<void> _submit() async {
//     setState(() {
//       _submitted = true;
//       _isLoading = true;
//     });
//     try {
//       final auth = Provider.of<AuthBase>(context, listen: false);
//       if (_formType == EmailSignInFormType.signIn) {
//         await auth.signinEmail(_email, _password);
//       } else {
//         await auth.createEmail(_email, _password);
//       }
//       Navigator.of(context).pop();
//     } on PlatformException catch (e) {
//       PlatformExeptionAlertDialog(
//         title: 'Sign in Fail!',
//         exception: e,
//       ).show(context);
//     } finally {
//       setState(() {
//         _isLoading = false;
//       });
//     }
//   }

//   void _emailEditingCompleted() {
//     final newFocus = widget.emaolValidator.isValid(_email)
//         ? _passwordFocusNode
//         : _emailFocusNode;
//     FocusScope.of(context).requestFocus(newFocus);
//   }

//   void _toggleForm() {
//     setState(() {
//       _submitted = false;
//       _formType = _formType == EmailSignInFormType.signIn
//           ? EmailSignInFormType.register
//           : EmailSignInFormType.signIn;
//     });
//     _emailController.clear();
//     _passwordController.clear();
//   }

//   List<Widget> _buildChildren() {
//     final primaryText = _formType == EmailSignInFormType.signIn
//         ? 'SignIn'
//         : 'create an account?';
//     final secondaryText = _formType == EmailSignInFormType.signIn
//         ? 'Need and acount? Register'
//         : 'Have an account? Sign in';
//     bool submitEnabled = widget.emaolValidator.isValid(_email) &&
//         widget.passwordValidator.isValid(_password) &&
//         !_isLoading;

//     return [
//       _buildEmailText(),
//       SizedBox(height: 8.0),
//       _buildPasswordText(),
//       SizedBox(height: 8.0),
//       FormSubmitButton(
//         text: primaryText,
//         onPressed: submitEnabled ? _submit : null,
//       ),
//       SizedBox(height: 8.0),
//       FlatButton(
//         child: Text(secondaryText),
//         onPressed: !_isLoading ? _toggleForm : null,
//       ),
//     ];
//   }

//   TextField _buildPasswordText() {
//     bool showErrorText =
//         _submitted && !widget.passwordValidator.isValid(_password);
//     return TextField(
//       controller: _passwordController,
//       focusNode: _passwordFocusNode,
//       decoration: InputDecoration(
//         labelText: 'Password',
//         errorText: showErrorText ? widget.passwordEroorNotValid : null,
//         enabled: _isLoading == false,
//       ),
//       obscureText: true,
//       textInputAction: TextInputAction.done,
//       onChanged: (password) => _updateState(),
//       onEditingComplete: _submit,
//     );
//   }

//   TextField _buildEmailText() {
//     bool showErrorText = _submitted && !widget.emaolValidator.isValid(_email);
//     return TextField(
//       controller: _emailController,
//       focusNode: _emailFocusNode,
//       decoration: InputDecoration(
//         labelText: 'Email',
//         hintText: 'your@email.com',
//         errorText: showErrorText ? widget.emailEroorNotValid : null,
//         enabled: _isLoading == false,
//       ),
//       autocorrect: false,
//       keyboardType: TextInputType.emailAddress,
//       textInputAction: TextInputAction.next,
//       onChanged: (email) => _updateState(),
//       onEditingComplete: _emailEditingCompleted,
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.all(16.0),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.stretch,
//         mainAxisSize: MainAxisSize.min,
//         children: _buildChildren(),
//       ),
//     );
//   }

//   void _updateState() {
//     setState(() {});
//   }
// }
