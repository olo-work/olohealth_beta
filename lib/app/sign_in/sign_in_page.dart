import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

// import '../../common_widgets/platform_exeption_alert_dialog.dart';
import '../../services/auth.dart';
import 'arc_clipper.dart';
import 'email_sign_in_page.dart';
import 'sign_in_button.dart';
import 'signin_bloc.dart';
// import 'social_signin_button.dart';

class SignInPage extends StatelessWidget {
  SignInPage({
    Key key,
    @required this.bloc,
    @required this.isLoading,
    this.showIcon = true,
    this.image,
  }) : super(key: key);
  final SignInBloc bloc;
  final bool isLoading;
  final showIcon;
  final image;

  static Widget create(BuildContext context) {
    final auth = Provider.of<AuthBase>(context);
    return ChangeNotifierProvider<ValueNotifier<bool>>(
      create: (_) => ValueNotifier<bool>(false),
      child: Consumer<ValueNotifier<bool>>(
        builder: (_, isLoading, __) => Provider<SignInBloc>(
          create: (_) => SignInBloc(auth: auth, isLoading: isLoading),
          child: Consumer<SignInBloc>(
            builder: (context, bloc, _) => SignInPage(
              bloc: bloc,
              isLoading: isLoading.value,
            ),
          ),
        ),
      ),
    );
  }

  // bool _isLoading = false;
  // void _showSigninError(BuildContext context, PlatformException exception) {
  //   PlatformExeptionAlertDialog(
  //     title: "Sign in failed",
  //     exception: exception,
  //   ).show(context);
  // }

  // Future<void> _signInAnonymously(BuildContext context) async {
  //   try {
  //     await bloc.signInAnonymously();
  //   } on PlatformException catch (e) {
  //     if (e.code != 'ERROR_ABORTED_BY_USER') {
  //       _showSigninError(context, e);
  //     }
  //   }
  // }

  // Future<void> _signinGoogle(BuildContext context) async {
  //   try {
  //     await bloc.signInGoogle();
  //   } on PlatformException catch (e) {
  //     if (e.code != 'ERROR_ABORTED_BY_USER') {
  //       _showSigninError(context, e);
  //     }
  //   }
  // }

  // Future<void> _signinFacebook(BuildContext context) async {
  //   try {
  //     await bloc.signInFacebook();
  //   } on PlatformException catch (e) {
  //     if (e.code != 'ERROR_ABORTED_BY_USER') {
  //       _showSigninError(context, e);
  //     }
  //   }
  // }

  void _signinEmail(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        fullscreenDialog: true,
        builder: (context) => EmailSignInPage(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.transparent,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        // title: Text(
        //   'OLO Health',
        //   style: TextStyle(
        //     color: Colors.black,
        //     fontSize: 16,
        //   ),
        // ),
        actions: <Widget>[
          FlatButton(
            child: Text(
              'Home',
              style: TextStyle(
                fontSize: 16.0,
                color: Colors.black,
              ),
            ),
            onPressed: () {},
          ),
          FlatButton(
            child: Text(
              'About us',
              style: TextStyle(
                fontSize: 16.0,
                color: Colors.black,
              ),
            ),
            onPressed: () {},
          ),
          // FlatButton(
          //   child: Text(
          //     'Login',
          //     style: TextStyle(
          //       fontSize: 16.0,
          //       color: Colors.black,
          //     ),
          //   ),
          //   onPressed: isLoading ? null : () => _signinEmail(context),
          // ),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: RaisedButton(
              color: Colors.blue,
              child: Text(
                'EN',
                style: TextStyle(
                  fontSize: 16.0,
                  color: Colors.black,
                ),
              ),
              onPressed: () {},
            ),
          ),
        ],
        elevation: 0.0,
      ),
      body: Container(
        constraints: BoxConstraints.expand(),
        color: Colors.grey[200],
        child: Stack(
          children: <Widget>[
            new Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('images/background.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Positioned(
              bottom: 8,
              left: 30,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'by',
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.indigo[500] 
                    ),
                  ),
                  Image.asset(
                    'images/olo_8.png',
                    fit: BoxFit.cover,
                    height: MediaQuery.of(context).size.height / 13,
                  ),
                ],
              ),
            ),
            Row(
              children: [
                SizedBox(width: 100),
                Container(
                  // padding: const EdgeInsets.all(16.0),
                  child: _buildContent(context),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildContent(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        // mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _buildHeader(context),
          // SizedBox(height: 30.0),
          // SocialSignInButton(
          //   assetName: 'images/google-logo.png',
          //   text: 'Sign in with Google',
          //   textColor: Colors.black87,
          //   color: Colors.white,
          //   onPressed: isLoading ? null : () => _signinGoogle(context),
          // ),
          // SizedBox(height: 10.0),
          // SocialSignInButton(
          //   assetName: 'images/facebook-logo.png',
          //   text: 'Sign in with Facebook',
          //   textColor: Colors.white,
          //   color: Color(0xFF334D92),
          //   onPressed: isLoading ? null : () => _signinFacebook(context),
          // ),
          SizedBox(height: 40.0),
          SignInButton(
            text: 'Login',
            textColor: Colors.white,
            color: Colors.indigo[700],
            onPressed: isLoading ? null : () => _signinEmail(context),
            borderRadius: 10,
            height: 30,
          ),
          SizedBox(height: 5.0),
          Text(
            'V.0.0.10',
            // overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.start,
            style: TextStyle(
              color: Colors.black,
              fontSize: 10.0,
              fontWeight: FontWeight.w400,
            ),
          ),
          // Text(
          //   'or',
          //   style: TextStyle(fontSize: 15.0, color: Colors.black87),
          //   textAlign: TextAlign.center,
          // ),
          // SizedBox(height: 10.0),
          // SignInButton(
          //   text: 'Go anonymous',
          //   textColor: Colors.black87,
          //   color: Colors.limeAccent,
          //   onPressed: isLoading ? null : () => _signInAnonymously(context),
          // ),
        ],
      ),
    );
  }

  Widget _buildHeader(BuildContext context) {
    if (isLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    // return Container();
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Image.asset(
          'images/pratama.png',
          fit: BoxFit.cover,
          height: MediaQuery.of(context).size.height / 6,
        ),
        Text(
          'Klinik Pratama',
          // overflow: TextOverflow.ellipsis,
          textAlign: TextAlign.start,
          style: TextStyle(
            color: Colors.indigo[500],
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        Text(
          'GKI Pondok Indah',
          // overflow: TextOverflow.ellipsis,
          textAlign: TextAlign.start,
          style: TextStyle(
            color: Colors.indigo[500],
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        // Text(
        //   'Selamat datang, silahkan Login',
        //   // overflow: TextOverflow.ellipsis,
        //   textAlign: TextAlign.start,
        //   style: TextStyle(
        //     color: Colors.black,
        //     fontSize: 16.0,
        //     fontWeight: FontWeight.w400,
        //   ),
        // ),
        // Text(
        //   'V.0.0.4',
        //   // overflow: TextOverflow.ellipsis,
        //   textAlign: TextAlign.start,
        //   style: TextStyle(
        //     color: Colors.black,
        //     fontSize: 10.0,
        //     fontWeight: FontWeight.w400,
        //   ),
        // ),
        // Text(
        //   'di tombol kanan atas "Login"',
        //   // overflow: TextOverflow.ellipsis,
        //   textAlign: TextAlign.start,
        //   style: TextStyle(
        //     color: Colors.black,
        //     fontSize: 16.0,
        //     fontWeight: FontWeight.w400,
        //   ),
        // ),
      ],
    );
  }

  Widget topHalf(BuildContext context) {
    var deviceSize = MediaQuery.of(context).size;
    return new Flexible(
      flex: 2,
      child: ClipPath(
        clipper: new ArcClipper(),
        child: Stack(
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                gradient: new LinearGradient(
                  colors: <Color>[
                    Colors.blueGrey.shade800,
                    Colors.black87,
                  ],
                ),
              ),
            ),
            showIcon
                ? new Center(
                    child: SizedBox(
                      height: deviceSize.height / 8,
                      width: deviceSize.width / 2,
                      child: Image.asset('images/SM_logo.png'),
                    ),
                    // child: FlutterLogo(
                    //   colors: Colors.yellow,
                    // )),
                  )
                : new Container(
                    width: double.infinity,
                    child: image != null
                        ? Image.asset(
                            image,
                            fit: BoxFit.cover,
                          )
                        : new Container())
          ],
        ),
      ),
    );
  }

  final bottomHalf = new Flexible(
    flex: 3,
    child: new Container(),
  );
}
