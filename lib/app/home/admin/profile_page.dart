import 'package:olohealth/app/home/models/pasien.dart';
import 'package:olohealth/app/home/models/profile.dart';
import 'package:olohealth/app/home/models/vitalsign.dart';
import 'package:olohealth/app/home/profile/list_items_builder.dart';
import 'package:olohealth/app/home/admin/pasien_list.dart';
import 'package:olohealth/app/home/admin/pasien_page.dart';
import 'package:olohealth/common_widgets/platform_alert_dialog.dart';
import 'package:olohealth/services/auth.dart';
import 'package:olohealth/services/database.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({
    Key key,
    this.profile,
    this.user,
    this.database,
    this.pasien,
    this.vital,
  }) : super(key: key);
  final AuthBase user;
  final Profile profile;
  final Database database;
  final Pasien pasien;
  final Vital vital;

  Future<void> _signOut(BuildContext context) async {
    try {
      final auth = Provider.of<AuthBase>(context, listen: false);
      await auth.signOut();
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> _confirmSignOut(BuildContext context) async {
    final didRequestSignout = await PlatformAlertDialog(
      title: 'Log Out',
      content: 'are you sure you want to Logout',
      cancelAction: 'Cancel',
      defaultActionText: 'Logout',
    ).show(context);
    if (didRequestSignout == true) {
      _signOut(context);
    }
  }

  // Future<void> _delete(BuildContext context, User user) async {
  //   try {
  //     final database = Provider.of<Database>(context, listen: false);
  //     await database.deleteUser(user);
  //   } on PlatformException catch (e) {
  //     PlatformExeptionAlertDialog(
  //       title: 'operation failed',
  //       exception: e,
  //     ).show(context);
  //   }
  // }

  // Future<void> _delete2(BuildContext context, Pasien pasien) async {
  //   try {
  //     final database = Provider.of<Database>(context, listen: false);
  //     await database.deletePasien(pasien);
  //   } on PlatformException catch (e) {
  //     PlatformExeptionAlertDialog(
  //       title: 'operation failed',
  //       exception: e,
  //     ).show(context);
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<AuthBase>(context, listen: false);
    return FutureBuilder<User>(
      future: auth.currentUser(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final user = snapshot.data;
          final adminId = user?.uid;
          print('admin Id: ' + adminId);
          if (adminId.isNotEmpty) {
            final database = Provider.of<Database>(context, listen: false);
            return StreamBuilder<User>(
              stream: database.readUser(userId: adminId),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  final _user = snapshot.data;
                  final adminName = _user?.name;
                  final avatar = _user?.avatar;
                  return DefaultTabController(
                    length: 2,
                    child: Scaffold(
                      extendBodyBehindAppBar: false,
                      backgroundColor: Colors.black54,
                      appBar: PreferredSize(
                        preferredSize: Size.fromHeight(
                            MediaQuery.of(context).size.height / 4),
                        child: AppBar(
                          flexibleSpace: FlexibleSpaceBar(
                            stretchModes: <StretchMode>[
                              StretchMode.zoomBackground,
                              StretchMode.blurBackground,
                              StretchMode.fadeTitle,
                            ],
                            centerTitle: true,
                            title: Center(
                              child: Column(
                                children: [
                                  SizedBox(height: 25),
                                  Image.asset(
                                    '$avatar',
                                    fit: BoxFit.cover,
                                    height: MediaQuery.of(context).size.height *
                                        0.1,
                                  ),
                                  // SizedBox(height: 8),
                                  Text(
                                    adminName,
                                    style: TextStyle(
                                      fontSize: 22,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          bottom: TabBar(
                            // labelPadding: EdgeInsets.all(30.0),
                            tabs: [
                              Tab(
                                  icon: Icon(Icons.people),
                                  text: 'Appointment'),
                              Tab(
                                  icon: Icon(Icons.description),
                                  text: 'Pasien List'),
                            ],
                          ),
                          elevation: 2.0,
                          backgroundColor: Colors.black54,
                          // title: Text(adminName),
                          actions: <Widget>[
                            FlatButton(
                              child: Text(
                                'Log out',
                                style: TextStyle(
                                  fontSize: 18.0,
                                  color: Colors.white,
                                ),
                              ),
                              onPressed: () => _confirmSignOut(context),
                            )
                          ],
                        ),
                      ),
                      body: Stack(
                        children: [
                          TabBarView(
                            children: [
                              _buildContents(context),
                              _buildContents2(context),
                            ],
                          ),
                        ],
                      ),
                      // floatingActionButton: FloatingActionButton(
                      //   child: Icon(Icons.person_add),
                      //   onPressed: () => SetPasienPage.show(
                      //     context,
                      //     database:
                      //         Provider.of<Database>(context, listen: false),
                      //   ),
                      // ),
                    ),
                  );
                }
                return CircularProgressIndicator();
              },
            );
          }
          return CircularProgressIndicator();
        }
        return CircularProgressIndicator();
      },
    );
  }

  Widget _buildContents(BuildContext context) {
    final database = Provider.of<Database>(context, listen: false);
    final int query = new DateTime(
      DateTime.now().year,
      DateTime.now().month,
      DateTime.now().day,
    ).millisecondsSinceEpoch;
    // print(query);
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.note_add),
        onPressed: () => SetPasienPage.show(
          context,
          database: Provider.of<Database>(context, listen: false),
        ),
      ),
      body: Stack(
        children: [
          new Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/doctor.png'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          new Container(
            color: Colors.black54,
            constraints: BoxConstraints.expand(),
          ),
          StreamBuilder<List<Pasien>>(
            stream: database.queryPasien(date: query),
            builder: (context, snapshot) {
              return ListItemsBuilder<Pasien>(
                snapshot: snapshot,
                itemBuilder: (context, pasien) => PasienList(
                  user: pasien,
                  onTap: () => SetPasienPage.show2(
                      context: context, database: database, pasien: pasien),
                ),
              );
            },
          ),
        ],
      ),
    );
  }

  Widget _buildContents2(BuildContext context) {
    final database = Provider.of<Database>(context, listen: false);
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.person_add),
        onPressed: () => SetPasienPage.show(
          context,
          vital: vital,
          database: Provider.of<Database>(context, listen: false),
        ),
      ),
      body: Stack(
        children: [
          new Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/doctor.png'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          new Container(
            color: Colors.black54,
            constraints: BoxConstraints.expand(),
          ),
          StreamBuilder<List<Pasien>>(
            stream: database.readPasien(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
              // print(pasien);
              return ListItemsBuilder<Pasien>(
                snapshot: snapshot,
                itemBuilder: (context, pasien) => PasienList(
                  user: pasien,
                  onTap: () => SetPasienPage.show2(
                      context: context, database: database, pasien: pasien, vital: vital),
                ),
              );
              }
              print('error disini buildcontents2');
              return Scaffold(
                body: Center(child: CircularProgressIndicator()),
              );
            },
          ),
        ],
      ),
    );
  }
}
