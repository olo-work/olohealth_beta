import 'package:olohealth/app/home/medical/format.dart';
import 'package:olohealth/app/home/models/konsultasi.dart';
import 'package:flutter/material.dart';

class ConsultList extends StatelessWidget {
  const ConsultList({Key key, @required this.consult, this.onTap})
      : super(key: key);

  final Konsultasi consult;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    // return ListTile(
    //   title: Text(sexslave.name),
    //   onTap: onTap,

    // );
    return Card(
      color: Colors.black12,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: InkWell(
          child: _buildText(context),
          onTap: onTap,
        ),
      ),
    );
  }

  Widget _buildText(BuildContext context) {
    final ageInt = consult.pasienAge.toStringAsFixed(0);
    final appoDate = Format.date(consult.tanggal);
    final dayOfWeek = Format.dayOfWeek(consult.tanggal);
    final startTime = TimeOfDay.fromDateTime(consult.tanggal).format(context);
    return ListTile(
      leading: Image.asset(
        'images/audrey.png',
        fit: BoxFit.cover,
      ),
      title: Text(
        '${consult.pasienName} | $ageInt',
        style: TextStyle(
          fontSize: 24,
          color: Colors.white,
        ),
      ),
      subtitle: Row(
        children: [
          Text(
            'Have a appointment at',
            style: TextStyle(
              fontSize: 18,
              color: Colors.white,
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Text(
            dayOfWeek,
            style: TextStyle(
              fontSize: 18,
              color: Colors.white,
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Text(
            appoDate,
            style: TextStyle(
              fontSize: 18,
              color: Colors.white,
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Text(
            startTime,
            style: TextStyle(
              fontSize: 18,
              color: Colors.white,
            ),
          ),
          // SizedBox(width: 20,),
          // Text(
          //   'with Doctor -',
          //   style: TextStyle(
          //     fontSize: 18,
          //     color: Colors.white,
          //   ),
          // ),
          // SizedBox(
          //   width: 10,
          // ),
          // Text(
          //   '${user.withDoc}',
          //   style: TextStyle(
          //     fontSize: 18,
          //     color: Colors.white,
          //   ),
          // ),
          // SizedBox(
          //   width: 10,
          // ),
        ],
      ),
    );
  }
}
