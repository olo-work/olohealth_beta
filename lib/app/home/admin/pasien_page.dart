import 'dart:async';
import 'package:dropdown_formfield/dropdown_formfield.dart';
import 'package:flutter/foundation.dart';
import 'package:olohealth/app/home/medical/date_time_picker.dart';
import 'package:olohealth/app/home/models/pasien.dart';
import 'package:olohealth/app/home/models/vitalsign.dart';
import 'package:olohealth/app/home/perawat/vitalsign_page.dart';
import 'package:olohealth/app/sign_in/sign_in_button.dart';
import 'package:olohealth/common_widgets/platform_exeption_alert_dialog.dart';
import 'package:olohealth/services/Time_picker.dart';
import 'package:olohealth/services/auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:olohealth/services/database.dart';

class SetPasienPage extends StatefulWidget {
  const SetPasienPage({
    Key key,
    @required this.database,
    this.pasien,
    this.user,
    this.vital,
  }) : super(key: key);
  final Database database;
  final Pasien pasien;
  final User user;
  final Vital vital;

  static Future<void> show(BuildContext context,
      {Database database, Pasien pasien, Vital vital}) async {
    await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => SetPasienPage(
          database: database,
          pasien: pasien,
          vital: vital,
        ),
        fullscreenDialog: true,
      ),
    );
  }

  static Future<void> show2({
    BuildContext context,
    Database database,
    Pasien pasien,
    User user,
    Vital vital,
  }) async {
    await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => SetPasienPage(
          database: database,
          pasien: pasien,
          user: user,
          vital: vital,
        ),
        fullscreenDialog: true,
      ),
    );
  }

  @override
  _SetPasienPageState createState() => _SetPasienPageState();
}

enum Doctor1 { salim, andrea, tiffany, audrey }

class _SetPasienPageState extends State<SetPasienPage> {
  final _formKey = GlobalKey<FormState>();
  String _name;
  String _gender;
  double _age;
  String _adress;
  String _phone;
  String _handphone;
  String _pekerjaan;
  String _alergi;
  DateTime _ttlDate = DateTime.now();
  String _templahir;
  String _avatar = 'images/yesaya.png';
  // String _withDoc;
  DateTime _startDate = DateTime.now();
  TimeOfDay _startTime = TimeOfDay.now();
  TimeOfDay _ttlTime = TimeOfDay.now();
  String _selection;
  String _idPasien = 'default';
  String _dokter;

  @override
  void initState() {
    super.initState();
    if (widget.pasien != null) {
      print('pasien not null');
      final appointment = widget.pasien?.appoDate ?? DateTime.now();
      final _ttl = widget.pasien?.ttl ?? DateTime.now();
      _name = widget.pasien.name;
      _idPasien = widget.pasien.idPasien;
      _gender = widget.pasien.gender;
      _age = widget.pasien.age;
      _adress = widget.pasien.adress;
      _phone = widget.pasien.phone;
      _handphone = widget.pasien.handphone;
      _pekerjaan = widget.pasien.pekerjaan;
      _alergi = widget.pasien.alergi;
      _templahir = widget.pasien.templahir;
      _avatar = widget.pasien.avatar;
      _selection = widget.pasien.withDoc;
      _startDate =
          DateTime(appointment.year, appointment.month, appointment.day);
      _startTime = TimeOfDay.fromDateTime(appointment);
      _ttlDate = DateTime(_ttl.year, _ttl.month, _ttl.day);
      _ttlTime = TimeOfDay.fromDateTime(_ttl);
    }
  }

  bool _saveForm() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Future<void> _submit() async {
    if (_saveForm()) {
      try {
        // final pasiens = await widget.database.readPasien().first;
        // final allNames = pasiens.map((e) => e.name).toList();
        // if (widget.pasien != null) {
        //   allNames.remove(widget.pasien.name);
        // }
        // if (allNames.contains(_name)) {
        //   PlatformAlertDialog(
        //     title: 'name already used',
        //     content: 'please use a diffrent pasien names',
        //     defaultActionText: 'OK',
        //   ).show(context);
        // } else {
        final appoDate = DateTime(_startDate.year, _startDate.month,
            _startDate.day, _startTime.hour, _startTime.minute);
        final queryDate =
            DateTime(_startDate.year, _startDate.month, _startDate.day);
        final ttl = DateTime(_ttlDate.year, _ttlDate.month, _ttlDate.day,
            _ttlTime.hour, _ttlTime.minute);
        final id = widget.pasien?.id ?? documentIdFromCurrentDate();
        final uid = widget.pasien?.uid ?? documentIdFromCurrentDate();
        final pasien = Pasien(
          id: id,
          uid: uid,
          idPasien: _idPasien,
          name: _name,
          adress: _adress,
          gender: _gender,
          age: _age,
          phone: _phone,
          handphone: _handphone,
          pekerjaan: _pekerjaan,
          alergi: _alergi,
          ttl: ttl,
          templahir: _templahir,
          avatar: _avatar,
          appoDate: appoDate,
          withDoc: _selection,
          queryDate: queryDate,
          vitalId: id,
        );
        final vital = Vital(
          id: id,
          pasienId: id,
          // berat: 0,
          // tinggi: 0,
          // bmi: 0,
          // tekanandarah: '0/0',
          // nadi: 0,
          // respiration: 0,
          // spo2: 0,
          // temp: 0,
          // gula: 0,
          lastCheck: DateTime.now(),
        );
        if (widget.pasien == null) {
          // print('vitalsign null');
          await widget.database.setVital(vital);
        }
        // await widget.database.setVital(vital);
        await widget.database.setPasien(pasien);
        Navigator.of(context).pop();
        // }
      } on PlatformException catch (e) {
        PlatformExeptionAlertDialog(
          title: 'operation failed',
          exception: e,
        ).show(context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 2.0,
        title: Text(widget.pasien == null ? 'Add Pasien' : 'Edit Pasien'),
      ),
      body: _buildContent(),
      backgroundColor: Colors.grey[200],
    );
  }

  Widget _buildContent() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: _buildForm(),
          ),
        ),
      ),
    );
  }

  _buildForm() {
    // if (widget.pasien == null) {
    //   return Form(
    //     key: _formKey,
    //     child: Column(
    //       crossAxisAlignment: CrossAxisAlignment.stretch,
    //       children: _buildFormChildren(),
    //     ),
    //   );
    // }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: _buildFormChildren(),
          ),
        ),
        _buildAppointment(),
        SizedBox(height: 10.0),
        SignInButton(
          text: 'Done',
          textColor: Colors.white,
          color: Colors.indigo[500],
          onPressed: _submit,
          height: 50,
          borderRadius: 10,
        ),
      ],
    );
  }

  Widget _buildAppointment() {
    // print(widget.user.name);
    return StreamBuilder<List<User>>(
        stream: widget.database.usersStream(role: 'doctor'),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final user = snapshot.data;
            return Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Text(
                  'Janji Konsultasi',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 15),
                // Text('Pilih Tanggal Konsultasi'),
                // SizedBox(height: 20),
                _buildAppoDate(),
                InputDecorator(
                  decoration: const InputDecoration(
                    //labelText: 'Activity',
                    hintText: 'Pilih dokter',
                  ),
                  isEmpty: _selection == null,
                  child: DropdownButton(
                    value: _selection,
                    isExpanded: true,
                    onChanged: (String newValue) {
                      setState(() {
                        _selection = newValue;
                        // dropDown = false;
                        print(_dokter);
                      });
                    },
                    items: user.map(
                      (e) {
                        return DropdownMenuItem<String>(
                          value: e.uid,
                          child: Container(
                            decoration: BoxDecoration(
                              // color: Colors.blue,
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                            // height: 100.0,
                            padding: EdgeInsets.fromLTRB(10.0, 2.0, 10.0, 0.0),
                            //color: primaryColor,
                            child: Text(e.name),
                          ),
                        );
                      },
                    ).toList(),
                  ),
                ),
                // SizedBox(height: 20),
                // Text('Pilih Dokter'),
                // SizedBox(height: 20),
                // DropDownFormField(
                //   titleText: 'Pilih Dokter',
                //   hintText: 'Nama Dokter',
                //   value: _selection,
                //   onSaved: (value) {
                //     setState(() {
                //       _selection = value;
                //     });
                //   },
                //   onChanged: (value) {
                //     setState(() {
                //       _selection = value;
                //     });
                //   },
                //   dataSource: [
                //     {
                //       "display": "Dr Isa P",
                //       "value": "qAtAzWeABGcFHXDUJoefOzvWJ0h2",
                //     },
                //     {
                //       "display": "Dr Ojahan Hutajulu",
                //       "value": "VVD7go6ONKNj8poAx6d11Gr3TwH3",
                //     },
                //     {
                //       "display": "Dr Karel Hutajulu",
                //       "value": "IUgGYkbz93Pqak2WeDT4TZclYFc2",
                //     },
                //     {
                //       "display": "Dr Yesaya",
                //       "value": "MsWiNtf7ZgVfyTpIrqaTWsnDgjc2",
                //     },
                //     {
                //       "display": "Dr Rafael",
                //       "value": "OafNWKMyhtaGwpsNW1P69VIbtpx2",
                //     },
                //     {
                //       "display": "Dr Debora",
                //       "value": "0VxQOx4W8hQaKTJtOsyq7576bT13",
                //     },
                //     {
                //       "display": "Dr Yudi",
                //       "value": "yMlHHfhzkPM4zWabBlB3jPQqCBp1",
                //     },
                //     {
                //       "display": "Dr Hanna",
                //       "value": "bWmokkrfC4TvMC5lHNzk9EBTKV83",
                //     },
                //     {
                //       "display": "Dr Kristina",
                //       "value": "ImkCkxJGALdZpDbORyotspWVg4k2",
                //     },
                //   ],
                //   textField: 'display',
                //   valueField: 'value',
                // ),
                SizedBox(height: 20),
              ],
            );
          }
          return Container();
        });
  }

  DropdownButton _listAvatar() => DropdownButton<String>(
        items: [
          DropdownMenuItem(
            value: 'images/yesaya.png',
            child: Row(
              children: <Widget>[
                Image.asset(
                  'images/yesaya.png',
                  fit: BoxFit.cover,
                  height: 40,
                ),
                SizedBox(width: 10),
                Text('avatar pria'),
              ],
            ),
          ),
          DropdownMenuItem(
            value: 'images/profile.png',
            child: Row(
              children: <Widget>[
                Image.asset(
                  'images/profile.png',
                  fit: BoxFit.cover,
                  height: 40,
                ),
                SizedBox(width: 10),
                Text('avatar wanita'),
              ],
            ),
          ),
        ],
        onChanged: (value) {
          setState(() {
            _avatar = value;
          });
        },
        value: _avatar,
        isExpanded: true,
        itemHeight: 70,
        // hint: Text('Pilih Avatar'),
      );

  DropdownButton _listGender() => DropdownButton<String>(
        items: [
          DropdownMenuItem(
            value: 'Pria',
            child: Row(
              children: <Widget>[
                Icon(Icons.person),
                SizedBox(width: 10),
                Text('Pria'),
              ],
            ),
          ),
          DropdownMenuItem(
            value: 'Wanita',
            child: Row(
              children: <Widget>[
                Icon(Icons.person),
                SizedBox(width: 10),
                Text('Wanita'),
              ],
            ),
          ),
        ],
        onChanged: (value) {
          setState(() {
            _gender = value;
          });
        },
        value: _gender,
        isExpanded: true,
        // itemHeight: 70,
        hint: Text('Jenis kelamin'),
      );

  List<Widget> _buildFormChildren() {
    return <Widget>[
      TextFormField(
        decoration: InputDecoration(labelText: 'Nama'),
        initialValue: _name,
        validator: (value) =>
            value.isNotEmpty ? null : 'Nama tidak boleh kosong',
        onSaved: (value) => _name = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'ID Pasien'),
        initialValue: _idPasien,
        validator: (value) =>
            value.isNotEmpty ? null : 'ID Pasien tidak boleh Kosong',
        onSaved: (value) => _idPasien = value,
      ),
      _listGender(),
      // TextFormField(
      //   decoration: InputDecoration(labelText: 'Jenis Kelamin - Pria/Wanita'),
      //   initialValue: _gender,
      //   onSaved: (value) => _gender = value,
      // ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Alamat Rumah'),
        initialValue: _adress,
        onSaved: (value) => _adress = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Tempat Lahir'),
        initialValue: _templahir,
        onSaved: (value) => _templahir = value,
      ),
      SizedBox(height: 8.0),
      _buildTTLDate(),
      SizedBox(height: 8.0),
      TextFormField(
        decoration: InputDecoration(labelText: 'No Telp Rumah'),
        initialValue: _phone,
        onSaved: (value) => _phone = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'no Handphone'),
        initialValue: _handphone,
        onSaved: (value) => _handphone = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Pekerjaan'),
        initialValue: _pekerjaan,
        onSaved: (value) => _pekerjaan = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'alergi'),
        initialValue: _alergi,
        onSaved: (value) => _alergi = value,
      ),
      _listAvatar(),
      // TextFormField(
      //   decoration: InputDecoration(labelText: 'Avatar Url'),
      //   initialValue: _avatar,
      //   onSaved: (value) => _avatar = value,
      // ),
      // _buildAppoDate(),
      // _buildTTLDate(),
      SizedBox(height: 20.0),
    ];
  }

  Widget _buildAppoDate() {
    return DateTimePicker(
      labelText: 'Pilih tangal Janji Konsultasi',
      selectedDate: _startDate,
      selectedTime: _startTime,
      selectDate: (date) => setState(() => _startDate = date),
      selectTime: (time) => setState(() => _startTime = time),
    );
  }

  Widget _buildTTLDate() {
    return DatePicker(
      labelText: 'Tanggal Lahir',
      selectedDate: _ttlDate,
      selectDate: (date) => setState(() => _ttlDate = date),
    );
  }
}
