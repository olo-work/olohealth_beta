import 'dart:async';
import 'package:date_format/date_format.dart';
import 'package:flutter/foundation.dart';
import 'package:olohealth/app/home/medical/date_time_picker.dart';
import 'package:olohealth/app/home/models/entry.dart';
import 'package:olohealth/app/home/models/konsultasi.dart';
import 'package:olohealth/app/home/models/pasien.dart';
import 'package:olohealth/app/home/models/resep.dart';
import 'package:olohealth/app/home/models/vitalsign.dart';
import 'package:olohealth/app/sign_in/sign_in_button.dart';
import 'package:olohealth/common_widgets/platform_exeption_alert_dialog.dart';
import 'package:olohealth/services/Time_picker.dart';
import 'package:olohealth/services/auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:olohealth/services/database.dart';

class ConsultPage extends StatefulWidget {
  const ConsultPage({
    Key key,
    @required this.database,
    this.pasien,
    this.user,
    this.medical,
    this.resep,
    this.consult,
  }) : super(key: key);
  final Database database;
  final Pasien pasien;
  final User user;
  final Konsultasi consult;
  final Medical medical;
  final Resep resep;

  static Future<void> show(BuildContext context,
      {Database database,
      Pasien pasien,
      Konsultasi consult,
      User user,
      Medical medical,
      Resep resep}) async {
    await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => ConsultPage(
          database: database,
          consult: consult,
          pasien: pasien,
          user: user,
          medical: medical,
          resep: resep,
        ),
        fullscreenDialog: true,
      ),
    );
  }

  static Future<void> show2({
    BuildContext context,
    Database database,
    Konsultasi consult,
    Medical medical,
    Resep resep,
    Pasien pasien,
    User user,
  }) async {
    await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => ConsultPage(
          database: database,
          consult: consult,
          pasien: pasien,
          user: user,
          medical: medical,
          resep: resep,
        ),
        fullscreenDialog: true,
      ),
    );
  }

  @override
  _ConsultPageState createState() => _ConsultPageState();
}

enum Doctor1 { salim, andrea, tiffany, audrey }

class _ConsultPageState extends State<ConsultPage> {
  final _formKey = GlobalKey<FormState>();
  String _name;
  double _age;
  String _gender;
  String _pasienId;
  String _doctorId;
  String _doctor;
  String _resepId;
  String _medicalId;
  DateTime _tanggal = DateTime.now();
  DateTime _startDate = DateTime.now();
  TimeOfDay _startTime = TimeOfDay.now();

  @override
  void initState() {
    super.initState();
    if (widget.consult != null) {
      print('pasien not null');
      final appointment = widget.consult?.tanggal ?? DateTime.now();
      _name = widget.consult.pasienName;
      _age = widget.consult.pasienAge;
      _pasienId = widget.consult.pasienId;
      _doctor = widget.consult.doctorName;
      _doctorId = widget.consult.doctorId;
      _medicalId = widget.pasien.pekerjaan;
      _resepId = widget.pasien.alergi;
      _startDate =
          DateTime(appointment.year, appointment.month, appointment.day);
      _startTime = TimeOfDay.fromDateTime(appointment);
    }
  }

  bool _saveForm() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Future<void> _submit() async {
    if (_saveForm()) {
      try {
        // final pasiens = await widget.database.readPasien().first;
        // final allNames = pasiens.map((e) => e.name).toList();
        // if (widget.pasien != null) {
        //   allNames.remove(widget.pasien.name);
        // }
        // if (allNames.contains(_name)) {
        //   PlatformAlertDialog(
        //     title: 'name already used',
        //     content: 'please use a diffrent pasien names',
        //     defaultActionText: 'OK',
        //   ).show(context);
        // } else {
        final appoDate = DateTime(_startDate.year, _startDate.month,
            _startDate.day, _startTime.hour, _startTime.minute);
        final id = widget.consult?.id ?? documentIdFromCurrentDate();
        final consult = Konsultasi(
          id: id,
          pasienId: _pasienId,
          pasienName: _name,
          pasienAge: _age,
          pasienGender: _gender,
          doctorId: _doctorId,
          doctorName: _doctor,
          medicalId: _medicalId,
          resepId: _resepId,
          tanggal: appoDate,
        );
        // await widget.database.setVital(vital);
        await widget.database.setKonsultasi(consult);
        Navigator.of(context).pop();
        // }
      } on PlatformException catch (e) {
        PlatformExeptionAlertDialog(
          title: 'operation failed',
          exception: e,
        ).show(context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 2.0,
        title: Text(widget.consult == null ? 'Add Pasien' : 'Edit Pasien'),
      ),
      body: _buildContent(),
      backgroundColor: Colors.grey[200],
    );
  }

  Widget _buildContent() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: _buildForm(),
          ),
        ),
      ),
    );
  }

  _buildForm() {
    // if (widget.pasien == null) {
    //   return Form(
    //     key: _formKey,
    //     child: Column(
    //       crossAxisAlignment: CrossAxisAlignment.stretch,
    //       children: _buildFormChildren(),
    //     ),
    //   );
    // }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        _buildAppointment(),
        SizedBox(height: 10.0),
        SignInButton(
          text: 'Done',
          textColor: Colors.white,
          color: Colors.indigo[500],
          onPressed: _submit,
          height: 50,
          borderRadius: 10,
        ),
      ],
    );
  }

  Widget _buildAppointment() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Text(
          'Janji Konsultasi',
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(height: 15),
        // Text('Pilih Tanggal Konsultasi'),
        // SizedBox(height: 20),
        _buildAppoDate(),
        SizedBox(height: 20),
        _listDokter(),
        SizedBox(height: 20),
        _listPasien(),
      ],
    );
  }

  _listDokter() {
    return StreamBuilder<List<User>>(
      stream: widget.database.usersStream(role: 'doctor'),
      builder: (context, snapshot) {
        final user = snapshot.data;
        if (snapshot.hasData) {
          InputDecorator(
            decoration: const InputDecoration(
              //labelText: 'Activity',
              hintText: 'Pilih dokter',
            ),
            isEmpty: _doctorId == null,
            child: DropdownButton(
              value: _doctorId,
              isExpanded: true,
              onChanged: (String newValue) {
                setState(() {
                  _doctorId = newValue;
                  print(_doctorId);
                });
              },
              items: user.map(
                (e) {
                  return DropdownMenuItem<String>(
                    value: e.uid,
                    child: Container(
                      decoration: BoxDecoration(
                        // color: Colors.blue,
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      // height: 100.0,
                      padding: EdgeInsets.fromLTRB(10.0, 2.0, 10.0, 0.0),
                      //color: primaryColor,
                      child: Text(e.name),
                    ),
                    onTap: () {
                      setState(() {
                        _doctor = e.name;
                        print(_doctor);
                      });
                    },
                  );
                },
              ).toList(),
            ),
          );
        }
        return Container();
      },
    );
  }

  _listPasien() {
    return StreamBuilder<List<Pasien>>(
      stream: widget.database.queryPasien(date: DateTime.now().millisecondsSinceEpoch),
      builder: (context, snapshot) {
        final pasiens = snapshot.data;
        if (snapshot.hasData) {
          InputDecorator(
            decoration: const InputDecoration(
              //labelText: 'Activity',
              hintText: 'Pilih dokter',
            ),
            isEmpty: _pasienId == null,
            child: DropdownButton(
              value: _pasienId,
              isExpanded: true,
              onChanged: (String newValue) {
                setState(() {
                  _pasienId = newValue;
                  print(_pasienId);
                });
              },
              items: pasiens.map(
                (e) {
                  return DropdownMenuItem<String>(
                    value: e.id,
                    child: Container(
                      decoration: BoxDecoration(
                        // color: Colors.blue,
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      // height: 100.0,
                      padding: EdgeInsets.fromLTRB(10.0, 2.0, 10.0, 0.0),
                      //color: primaryColor,
                      child: Row(
                        children: [
                          Text(e.name),
                          SizedBox(width: 20),
                          Text(e.adress),
                          SizedBox(width: 20),
                          Text(e.handphone),
                        ],
                      ),
                    ),
                    onTap: () {
                      setState(() {
                        _name = e.name;
                        print(_name);
                      });
                    },
                  );
                },
              ).toList(),
            ),
          );
        }
        return Container();
      },
    );
  }

  Widget _buildAppoDate() {
    return DateTimePicker(
      labelText: 'Pilih tangal Janji Konsultasi',
      selectedDate: _startDate,
      selectedTime: _startTime,
      selectDate: (date) => setState(() => _startDate = date),
      selectTime: (time) => setState(() => _startTime = time),
    );
  }
}
