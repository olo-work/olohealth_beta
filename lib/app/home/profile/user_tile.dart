import 'package:olohealth/services/auth.dart';
import 'package:flutter/material.dart';

class UserTile extends StatelessWidget {
  const UserTile({Key key, @required this.user, this.onTap}) : super(key: key);

  final User user;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    // return ListTile(
    //   title: Text(sexslave.name),
    //   onTap: onTap,

    // );
    return Card(
      color: Colors.black12,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: InkWell(
          child: _buildText(),
          onTap: onTap,
        ),
      ),
    );
  }

  Widget _buildText() {
    return ListTile(
      title: Text(
        '${user.name}',
        style: TextStyle(
          fontSize: 18,
          color: Colors.white,
        ),
      ),
      subtitle: Text(
        '${user.uid}',
        style: TextStyle(
          fontSize: 10,
          color: Colors.white,
        ),
      ),
    );
  }
}
