import 'dart:async';

import 'package:olohealth/app/home/doctor/pasien_resep.dart';
import 'package:olohealth/app/home/doctor/resep_list.dart';
import 'package:olohealth/app/home/medical/add_medical2.dart';
import 'package:olohealth/app/home/medical/medical_list_item.dart';
import 'package:olohealth/app/home/models/entry.dart';
import 'package:olohealth/app/home/models/pasien.dart';
import 'package:olohealth/app/home/models/resep.dart';
import 'package:olohealth/app/home/models/vitalsign.dart';
import 'package:olohealth/app/home/perawat/vital_list.dart';
import 'package:olohealth/app/home/perawat/vitalsign_form.dart';
import 'package:olohealth/app/home/profile/list_items_builder.dart';
import 'package:olohealth/common_widgets/platform_exeption_alert_dialog.dart';
import 'package:olohealth/services/database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';

class VitalPage extends StatelessWidget {
  const VitalPage({
    @required this.database,
    @required this.pasien,
    this.vital,
  });
  final Database database;
  final Pasien pasien;
  final Vital vital;

  static Future<void> show(BuildContext context, Pasien pasien) async {
    final Database database = Provider.of<Database>(context, listen: false);
    await Navigator.of(context).push(
      MaterialPageRoute(
        fullscreenDialog: false,
        builder: (context) => VitalPage(database: database, pasien: pasien),
      ),
    );
  }

  Future<void> _deleteMedical(BuildContext context, Vital vital) async {
    try {
      await database.deleteVital(vital);
    } on PlatformException catch (e) {
      PlatformExeptionAlertDialog(
        title: 'Operation failed',
        exception: e,
      ).show(context);
    }
  }

  // _launchURL() async {
  //   const url = 'https://meet.jit.si/Klinik_pratama';
  //   if (await canLaunch(url)) {
  //     await launch(url);
  //   } else {
  //     throw 'Could not launch $url';
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // extendBodyBehindAppBar: true,
      backgroundColor: Colors.black54,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(250.0),
        child: AppBar(
          flexibleSpace: FlexibleSpaceBar(
            stretchModes: <StretchMode>[
              StretchMode.zoomBackground,
              StretchMode.blurBackground,
              StretchMode.fadeTitle,
            ],
            centerTitle: true,
            title: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(width: 50),
                  Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(50)),
                      image: DecorationImage(
                        image: AssetImage(pasien.avatar),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  SizedBox(width: 20),
                  Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Name'),
                        Text('Age'),
                        Text('Gender'),
                      ],
                    ),
                  ),
                  SizedBox(width: 50),
                  Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(': ' + pasien.name),
                        Text(': ' + pasien.age.toStringAsFixed(0)),
                        Text(': ' + pasien.gender),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          // leading: Padding(
          //   padding: const EdgeInsets.all(8.0),
          //   child: Image.asset(
          //     '${pasien.avatar}',
          //     fit: BoxFit.cover,
          //   ),
          // ),
          elevation: 2.0,
          backgroundColor: Colors.black54,
          // title: Text(pasien.name),
          // actions: <Widget>[
          //   IconButton(
          //     icon: Icon(Icons.videocam),
          //     onPressed: _launchURL,
          //   )
          // ],
        ),
      ),
      body: Stack(
        children: [
          new Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/doctor.png'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          new Container(
            color: Colors.black54,
            constraints: BoxConstraints.expand(),
          ),
          _buildContent(context, pasien),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.note_add),
        onPressed: () => VitalSignForm.show(
          context,
          database: database,
          pasien: pasien,
        ),
      ),
      // backgroundColor: Colors.transparent,
    );
  }

  Widget _buildContent(BuildContext context, Pasien pasien) { 
    // database.deleteVital(vital);
    // print(vital.id);
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: ListView(
        children: [
          StreamBuilder<List<Vital>>(
            stream: database.vitalStream(pasienId: pasien.id),
            builder: (context, snapshot) {
              return ListItemsBuilder<Vital>(
                snapshot: snapshot,
                itemBuilder: (context, vital) {
                  return DismissibleVitalList(
                    key: Key('medical-${vital.id}'),
                    pasien: pasien,
                    vital: vital,
                    onDismissed: () => _deleteMedical(context, vital),
                    onTap: () => VitalSignForm.show2(
                        context: context,
                        database: database,
                        vital: vital,
                        pasien: pasien),
                    // onTap: () => MedicalPage.show2(
                    //   context: context,
                    //   database: database,
                    //   profile: profile,
                    //   user: user,
                    // ),
                  );
                },
              );
            },
          ),
        ],
      ),
    );
  }
}
