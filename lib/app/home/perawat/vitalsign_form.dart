import 'dart:async';
import 'package:olohealth/app/home/models/vitalsign.dart';
import 'package:olohealth/app/home/models/pasien.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:olohealth/app/sign_in/sign_in_button.dart';
import 'package:olohealth/common_widgets/platform_exeption_alert_dialog.dart';
import 'package:olohealth/services/database.dart';

class VitalSignForm extends StatefulWidget {
  const VitalSignForm(
      {Key key, @required this.database, this.vital, this.pasien})
      : super(key: key);
  final Database database;
  final Vital vital;
  final Pasien pasien;

  static Future<void> show(BuildContext context,
      {Database database, Pasien pasien}) async {
    await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => VitalSignForm(
          database: database,
          pasien: pasien,
        ),
        fullscreenDialog: true,
      ),
    );
  }

  static Future<void> show2({
    BuildContext context,
    Database database,
    Vital vital,
    Pasien pasien,
  }) async {
    await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => VitalSignForm(
          database: database,
          vital: vital,
          pasien: pasien,
        ),
        fullscreenDialog: true,
      ),
    );
  }

  @override
  _VitalSignFormState createState() => _VitalSignFormState();
}

class _VitalSignFormState extends State<VitalSignForm> {
  final _formKey = GlobalKey<FormState>();
  // DateTime _end;
  double _berat;
  double _tinggi;
  double _bmi;
  String _tekanandarah;
  int _nadi;
  double _respiration;
  double _temp;
  double _spo2;
  double _gula;
  DateTime _lastDate = DateTime.now();

  @override
  void initState() {
    super.initState();
    print('my pasienId: ' + widget.pasien.id);
    if (widget.vital != null) {
      // _end = widget.medical.end;
      _berat = widget.vital.berat;
      _tinggi = widget.vital.tinggi;
      _bmi = widget.vital.bmi;
      _tekanandarah = widget.vital.tekanandarah;
      _nadi = widget.vital.nadi;
      _respiration = widget.vital.respiration;
      _temp = widget.vital.temp;
      _spo2 = widget.vital.spo2;
      _gula = widget.vital.gula;
      _lastDate = DateTime.now();
    }
  }

  bool _saveForm() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Future<void> _submit() async {
    print('Submit ID: ${widget.pasien?.id}');
    if (_saveForm()) {
      try {
        // final vitalss = await widget.database.medicalStream2().first;
        // final allNames = medicals.map((e) => e.keluhan1).toList();
        // if (widget.medical != null) {
        //   allNames.remove(widget.medical.keluhan1);
        // }
        // if (allNames.contains(_keluhan1)) {
        //   PlatformAlertDialog(
        //     title: 'name already used',
        //     content: 'please use a diffrent medical names',
        //     defaultActionText: 'OK',
        //   ).show(context);
        // } else {
          final id = widget.vital?.id ?? documentIdFromCurrentDate();
          // final lastDate = DateTime.now();
          final pasienId = widget.pasien.id;
          final vital = Vital(
            id: id,
            pasienId: pasienId,
            lastCheck: _lastDate,
            berat: _berat,
            tinggi: _tinggi,
            bmi: _berat / ((_tinggi / 100) * (_tinggi / 100)),
            tekanandarah: _tekanandarah,
            nadi: _nadi,
            respiration: _respiration,
            temp: _temp,
            spo2: _spo2,
            gula: _gula,
          );
          final pasien = Pasien(
            id: pasienId,
            vitalId: id,
          );
          await widget.database.updatePasien(pasien);
          await widget.database.setVital(vital);
          Navigator.of(context).pop();
        // }
      } on PlatformException catch (e) {
        PlatformExeptionAlertDialog(
          title: 'operation failed',
          exception: e,
        ).show(context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 2.0,
        title: Text(
            widget.pasien == null ? 'Buat Rekam Medis' : 'Edit Rekam Medis'),
        // actions: <Widget>[
        //   FlatButton(
        //     child: Text(
        //       'Save',
        //       style: TextStyle(fontSize: 18, color: Colors.white),
        //     ),
        //     onPressed: _submit,
        //   ),
        // ],
      ),
      // body: Stack(
      //   children: <Widget>[
      //     new Container(
      //       decoration: BoxDecoration(
      //         image: DecorationImage(
      //           image: AssetImage('images/suster.png'),
      //           fit: BoxFit.cover,
      //         ),
      //       ),
      //     ),
      //     new Container(
      //       child: _buildContent(),
      //     ),
      //   ],
      // ),
      body: _buildContent(),
      backgroundColor: Colors.grey[200],
    );
  }

  Widget _buildContent() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: _buildForm(),
          ),
        ),
      ),
    );
  }

  _buildForm() {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: _buildFormChildren(),
      ),
    );
  }

  _buildFormChildren() {
    return [
      TextFormField(
        decoration: InputDecoration(labelText: 'Berat Badan | Kg'),
        initialValue: _berat != null ? '$_berat' : null,
        onSaved: (value) => _berat = double.tryParse(value) ?? 0,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Tinggi Badan | cm'),
        initialValue: _tinggi != null ? '$_tinggi' : null,
        onSaved: (value) => _tinggi = double.tryParse(value) ?? 0,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Tekanan Darah'),
        initialValue: _tekanandarah,
        onSaved: (value) => _tekanandarah = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Nadi'),
        initialValue: _nadi != null ? '$_nadi' : null,
        onSaved: (value) => _nadi = int.tryParse(value) ?? 0,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Respiration rate'),
        initialValue: _respiration != null ? '$_respiration' : null,
        onSaved: (value) => _respiration = double.tryParse(value) ?? 0,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Temperature'),
        initialValue: _temp != null ? '$_temp' : null,
        onSaved: (value) => _temp = double.tryParse(value) ?? 0,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'SPO2'),
        initialValue: _spo2 != null ? '$_spo2' : null,
        onSaved: (value) => _spo2 = double.tryParse(value) ?? 0,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Gula Darah'),
        initialValue: _gula != null ? '$_gula' : null,
        onSaved: (value) => _gula = double.tryParse(value) ?? 0,
      ),
      SizedBox(height: 10.0),
      SignInButton(
        text: 'Submit',
        textColor: Colors.white,
        color: Colors.indigo[500],
        onPressed: _submit,
        height: 50,
        borderRadius: 10,
      ),
    ];
  }
}
