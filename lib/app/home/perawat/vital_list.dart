import 'package:olohealth/app/home/medical/format.dart';
import 'package:olohealth/app/home/models/pasien.dart';
import 'package:olohealth/app/home/models/vitalsign.dart';
import 'package:flutter/material.dart';

class VitalList extends StatelessWidget {
  const VitalList({
    @required this.pasien,
    @required this.vital,
    @required this.onTap,
  });

  final Pasien pasien;
  final Vital vital;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    // DateTime rekamDate = DateTime.parse(medical.id);
    return Container(
        height: MediaQuery.of(context).size.height * 0.7,
        child: InkWell(
          onTap: onTap,
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
            child: _buildContents(context),
          ),
        ),
      );
  }

  Widget _buildContents(BuildContext context) {
    final lastDate = Format.date(vital.lastCheck);
    return Card(
      child: Stack(
        children: [
          Container(
            color: Colors.black.withOpacity(0.7),
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  // SizedBox(width: 30),
                  Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Berat badan', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text('Tinggi badan', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text('BMI', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text('Tekanan Darah', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text('Nadi', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text('Respiration rate', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text('Temperature', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text('SPO2', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text('Gula Darah', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text('Last Update', style: TextStyle(fontSize: 20, color: Colors.white)),
                      ],
                    ),
                  ),
                  SizedBox(width: 30),
                  Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(': ${vital.berat} Kg', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text(': ${vital.tinggi} M', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text(': ${vital.bmi.toStringAsFixed(2)}', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text(': ${vital.tekanandarah}', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text(': ${vital.nadi}', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text(': ${vital.respiration}', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text(': ${vital.temp}', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text(': ${vital.spo2}', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text(': ${vital.gula}', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text(': $lastDate', style: TextStyle(fontSize: 20, color: Colors.white)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: 6,
            left: 10,
            child: Column(
              children: <Widget>[
                Text(
                  'Vital Sign - ' + lastDate,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 36,
                    fontWeight: FontWeight.w900,
                  ),
                ),
                const Divider(
                  color: Colors.grey,
                  height: 20,
                  thickness: 5,
                  indent: 10,
                  endIndent: 10,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class DismissibleVitalList extends StatelessWidget {
  const DismissibleVitalList({
    this.key,
    this.pasien,
    this.vital,
    this.onDismissed,
    this.onTap,
  });

  final Key key;
  final Pasien pasien;
  final Vital vital;
  final VoidCallback onDismissed;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      background: Container(color: Colors.red),
      key: key,
      direction: DismissDirection.endToStart,
      onDismissed: (direction) => onDismissed(),
      child: VitalList(
        pasien: pasien,
        vital: vital,
        onTap: onTap,
      ),
    );
  }
}
