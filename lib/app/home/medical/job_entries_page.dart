import 'dart:async';
import 'dart:html' as html;
import 'dart:html';
import 'package:olohealth/app/home/doctor/pasien_resep.dart';
import 'package:olohealth/app/home/doctor/resep_list.dart';
import 'package:olohealth/app/home/medical/add_medical2.dart';
import 'package:olohealth/app/home/models/entry.dart';
import 'package:olohealth/app/home/models/pasien.dart';
import 'package:olohealth/app/home/models/resep.dart';
import 'package:olohealth/app/home/models/vitalsign.dart';
import 'package:olohealth/app/home/profile/list_items_builder.dart';
import 'package:olohealth/common_widgets/platform_exeption_alert_dialog.dart';
import 'package:olohealth/services/database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'medical_list_item.dart';

class ProfileMedicalPage extends StatelessWidget {
  ProfileMedicalPage({
    @required this.database,
    @required this.pasien,
    this.medical,
    this.resep,
    this.vital,
  });
  final Database database;
  final Pasien pasien;
  final Medical medical;
  final Resep resep;
  final Vital vital;

  static Future<void> show(BuildContext context, Pasien pasien) async {
    final Database database = Provider.of<Database>(context, listen: false);
    await Navigator.of(context).push(
      MaterialPageRoute(
        fullscreenDialog: false,
        builder: (context) =>
            ProfileMedicalPage(database: database, pasien: pasien),
      ),
    );
  }

  Future<void> _deleteResep(BuildContext context, Resep resep) async {
    try {
      await database.deleteResep(resep);
    } on PlatformException catch (e) {
      PlatformExeptionAlertDialog(
        title: 'Operation failed',
        exception: e,
      ).show(context);
    }
  }

  Future<void> _deleteMedical(BuildContext context, Medical medical) async {
    try {
      await database.deleteMedical(medical);
    } on PlatformException catch (e) {
      PlatformExeptionAlertDialog(
        title: 'Operation failed',
        exception: e,
      ).show(context);
    }
  }

  _launchURL() async {
    const url = 'https://meet.jit.si/Klinik_pratama';
    // const url = 'https://flutter.io';
    if (await canLaunch(url)) {
      await launch(url,
          forceSafariVC: true, forceWebView: true, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  // _launchHtml() {
  //   // const url = 'https://meet.jit.si/Klinik_pratama';
  //   const url = 'https://flutter.io';
  //   const name = 'test';
  //   html.window.open(url, name);
  // }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Vital>(
        stream: database.readVital(userId: pasien.vitalId),
        builder: (context, snapshot) {
          final vital = snapshot.data;
          if (vital != null) {
            final bmi =
                vital.bmi == null ? 'null' : vital.bmi.toStringAsFixed(2);
            final height =
                vital.tinggi == null ? 'null' : vital.tinggi.toStringAsFixed(0);
            final weight =
                vital.berat == null ? 'null' : vital.berat.toStringAsFixed(0);
            final darah =
                vital.tekanandarah == null ? 'null' : vital.tekanandarah;
            final nadi = vital.nadi == null ? 'null' : vital.nadi.toString();
            final temp =
                vital.temp == null ? 'null' : vital.temp.toStringAsFixed(0);
            final spo2 =
                vital.spo2 == null ? 'null' : vital.spo2.toStringAsFixed(0);
            final respi = vital.respiration == null
                ? 'null'
                : vital.respiration.toStringAsFixed(0);
            print('vital not null');
            return DefaultTabController(
              length: 4,
              child: Scaffold(
                // extendBodyBehindAppBar: true,
                backgroundColor: Colors.black54,
                appBar: PreferredSize(
                  preferredSize: Size.fromHeight(
                      MediaQuery.of(context).size.height * 0.27),
                  child: AppBar(
                    flexibleSpace: FlexibleSpaceBar(
                      stretchModes: <StretchMode>[
                        StretchMode.zoomBackground,
                        StretchMode.blurBackground,
                        StretchMode.fadeTitle,
                      ],
                      centerTitle: false,
                      title: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(width: 50),
                          Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: Container(
                              width: 80,
                              height: 80,
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(40)),
                                image: DecorationImage(
                                  image: AssetImage(pasien.avatar),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(width: 20),
                          Padding(
                            padding: const EdgeInsets.only(top: 5),
                            child: IconButton(
                              color: Colors.blue,
                              iconSize: 35,
                              icon: Icon(Icons.videocam),
                              onPressed: _launchURL,
                              // onPressed: _launchWeb,
                            ),
                          ),
                          SizedBox(width: 20),
                          Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 20),
                                Row(
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text('Name'),
                                        Text('Age'),
                                        Text('Gender'),
                                      ],
                                    ),
                                    SizedBox(width: 20),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(': ' + pasien.name),
                                        Text(': ' +
                                            pasien.age.toStringAsFixed(0)),
                                        Text(': ' + pasien.gender),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          SizedBox(width: 50),
                          Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 20),
                                Row(
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text('BMI'),
                                        Text('Height'),
                                        Text('Weight'),
                                        Text('Tekanan Darah'),
                                      ],
                                    ),
                                    SizedBox(width: 20),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(': ' + bmi),
                                        Text(': ' + height + ' cm'),
                                        Text(': ' + weight + ' Kg'),
                                        Text(': ' + darah),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          SizedBox(width: 50),
                          Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 20),
                                Row(
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text('Nadi'),
                                        Text('Respiration Rate'),
                                        Text('SPO2'),
                                        Text('Temperature'),
                                      ],
                                    ),
                                    SizedBox(width: 20),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(': ' + nadi),
                                        Text(': ' + respi),
                                        Text(': ' + spo2),
                                        Text(': ' + temp),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      // background: Stack(
                      //   fit: StackFit.expand,
                      //   children: [
                      //     Image.asset(
                      //       '${pasien.avatar}',
                      //       fit: BoxFit.cover,
                      //     ),
                      //     const DecoratedBox(
                      //       decoration: BoxDecoration(
                      //         gradient: LinearGradient(
                      //           begin: Alignment(0.0, 0.5),
                      //           end: Alignment(0.0, 0.0),
                      //           colors: <Color>[
                      //             Color(0x60000000),
                      //             Color(0x00000000),
                      //           ],
                      //         ),
                      //       ),
                      //     ),
                      //   ],
                      // ),
                    ),
                    bottom: TabBar(
                      // labelPadding: EdgeInsets.all(30.0),
                      tabs: [
                        Tab(
                          icon: Icon(Icons.description),
                          text: 'Rekam Medis',
                        ),
                        Tab(
                          icon: Icon(Icons.donut_large),
                          text: 'Resep',
                        ),
                        Tab(
                          icon: Icon(Icons.photo_library),
                          text: 'Hasil Lab',
                        ),
                        Tab(
                          icon: Icon(Icons.comment),
                          text: 'Appointment',
                        ),
                      ],
                    ),
                    // leading: Padding(
                    //   padding: const EdgeInsets.all(8.0),
                    //   child: Image.asset(
                    //     '${pasien.avatar}',
                    //     fit: BoxFit.cover,
                    //   ),
                    // ),
                    elevation: 2.0,
                    backgroundColor: Colors.black54,
                    // title: Text(pasien.name),
                  ),
                ),
                body: Stack(
                  children: [
                    new Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('images/doctor.png'),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    new Container(
                      color: Colors.black54,
                      constraints: BoxConstraints.expand(),
                    ),
                    TabBarView(
                      children: [
                        _buildContent(context, pasien),
                        _buildContent2(context, pasien),
                        _buildContent3(context, pasien),
                        _buildContent4(context),
                      ],
                    ),
                  ],
                ),
                // floatingActionButton: FloatingActionButton(
                //   child: Icon(Icons.add),
                //   onPressed: () => SetMedicalPage.show2(
                //       context: context, database: database, pasien: pasien),
                // ),
                // backgroundColor: Colors.transparent,
              ),
            );
          }
          return Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
            backgroundColor: Colors.transparent,
          );
        });
  }

  Widget _buildContent4(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {},
      ),
      body: Stack(
        children: [
          new Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/doctor.png'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          new Container(
            color: Colors.black54,
            constraints: BoxConstraints.expand(),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: ListView(
              padding: EdgeInsets.all(10),
              children: [
                Container(
                  height: 100,
                  color: Colors.black54,
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Row(
                      children: [
                        Text('pasien name',
                            style:
                                TextStyle(color: Colors.white, fontSize: 20)),
                        SizedBox(width: 10),
                        Text('doctor name',
                            style:
                                TextStyle(color: Colors.white, fontSize: 20)),
                        SizedBox(width: 10),
                        Text('tanggal konsultasi',
                            style:
                                TextStyle(color: Colors.white, fontSize: 20)),
                        SizedBox(width: 10),
                        Text('status konsultasi',
                            style:
                                TextStyle(color: Colors.white, fontSize: 20)),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Container(
                  height: 100,
                  color: Colors.black54,
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Row(
                      children: [
                        Text('pasien name',
                            style:
                                TextStyle(color: Colors.white, fontSize: 20)),
                        SizedBox(width: 10),
                        Text('doctor name',
                            style:
                                TextStyle(color: Colors.white, fontSize: 20)),
                        SizedBox(width: 10),
                        Text('tanggal konsultasi',
                            style:
                                TextStyle(color: Colors.white, fontSize: 20)),
                        SizedBox(width: 10),
                        Text('status konsultasi',
                            style:
                                TextStyle(color: Colors.white, fontSize: 20)),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Container(
                  height: 100,
                  color: Colors.black54,
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Row(
                      children: [
                        Text('pasien name',
                            style:
                                TextStyle(color: Colors.white, fontSize: 20)),
                        SizedBox(width: 10),
                        Text('doctor name',
                            style:
                                TextStyle(color: Colors.white, fontSize: 20)),
                        SizedBox(width: 10),
                        Text('tanggal konsultasi',
                            style:
                                TextStyle(color: Colors.white, fontSize: 20)),
                        SizedBox(width: 10),
                        Text('status konsultasi',
                            style:
                                TextStyle(color: Colors.white, fontSize: 20)),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildContent3(BuildContext context, Pasien pasien) {
    return Stack(
      children: [
        new Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('images/doctor.png'),
              fit: BoxFit.cover,
            ),
          ),
        ),
        new Container(
          color: Colors.black54,
          constraints: BoxConstraints.expand(),
        ),
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: GridView.count(
            primary: false,
            padding: const EdgeInsets.all(20.0),
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
            crossAxisCount: 3,
            children: <Widget>[
              new GridTile(
                  footer: GestureDetector(
                    onTap: () {},
                    child: GridTileBar(
                      backgroundColor: Colors.black45,
                      title: FittedBox(
                        alignment: Alignment.centerLeft,
                        fit: BoxFit.scaleDown,
                        child: Text('Title'),
                      ),
                      subtitle: FittedBox(
                        alignment: Alignment.centerLeft,
                        fit: BoxFit.scaleDown,
                        child: Text('SubTitle'),
                      ),
                      trailing: Icon(
                        Icons.star,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  child: Image.asset(
                    'images/SM_logo.png',
                    fit: BoxFit.cover,
                  )),
              new GridTile(
                  footer: GestureDetector(
                    onTap: () {},
                    child: GridTileBar(
                      backgroundColor: Colors.black45,
                      title: FittedBox(
                        alignment: Alignment.centerLeft,
                        fit: BoxFit.scaleDown,
                        child: Text('Title'),
                      ),
                      subtitle: FittedBox(
                        alignment: Alignment.centerLeft,
                        fit: BoxFit.scaleDown,
                        child: Text('SubTitle'),
                      ),
                      trailing: Icon(
                        Icons.star,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  child: Image.asset(
                    'images/SM_logo.png',
                    fit: BoxFit.cover,
                  )),
              new GridTile(
                  footer: GestureDetector(
                    onTap: () {},
                    child: GridTileBar(
                      backgroundColor: Colors.black45,
                      title: FittedBox(
                        alignment: Alignment.centerLeft,
                        fit: BoxFit.scaleDown,
                        child: Text('Title'),
                      ),
                      subtitle: FittedBox(
                        alignment: Alignment.centerLeft,
                        fit: BoxFit.scaleDown,
                        child: Text('SubTitle'),
                      ),
                      trailing: Icon(
                        Icons.star,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  child: Image.asset(
                    'images/SM_logo.png',
                    fit: BoxFit.cover,
                  )),
              new GridTile(
                  footer: GestureDetector(
                    onTap: () {},
                    child: GridTileBar(
                      backgroundColor: Colors.black45,
                      title: FittedBox(
                        alignment: Alignment.centerLeft,
                        fit: BoxFit.scaleDown,
                        child: Text('Title'),
                      ),
                      subtitle: FittedBox(
                        alignment: Alignment.centerLeft,
                        fit: BoxFit.scaleDown,
                        child: Text('SubTitle'),
                      ),
                      trailing: Icon(
                        Icons.star,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  child: Image.asset(
                    'images/SM_logo.png',
                    fit: BoxFit.cover,
                  )),
              new GridTile(
                  footer: GestureDetector(
                    onTap: () {},
                    child: GridTileBar(
                      backgroundColor: Colors.black45,
                      title: FittedBox(
                        alignment: Alignment.centerLeft,
                        fit: BoxFit.scaleDown,
                        child: Text('Title'),
                      ),
                      subtitle: FittedBox(
                        alignment: Alignment.centerLeft,
                        fit: BoxFit.scaleDown,
                        child: Text('SubTitle'),
                      ),
                      trailing: Icon(
                        Icons.star,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  child: Image.asset(
                    'images/SM_logo.png',
                    fit: BoxFit.cover,
                  )),
              new GridTile(
                  footer: GestureDetector(
                    onTap: () {},
                    child: GridTileBar(
                      backgroundColor: Colors.black45,
                      title: FittedBox(
                        alignment: Alignment.centerLeft,
                        fit: BoxFit.scaleDown,
                        child: Text('Title'),
                      ),
                      subtitle: FittedBox(
                        alignment: Alignment.centerLeft,
                        fit: BoxFit.scaleDown,
                        child: Text('SubTitle'),
                      ),
                      trailing: Icon(
                        Icons.star,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  child: Image.asset(
                    'images/SM_logo.png',
                    fit: BoxFit.cover,
                  )),
              new GridTile(
                  footer: GestureDetector(
                    onTap: () {},
                    child: GridTileBar(
                      backgroundColor: Colors.black45,
                      title: FittedBox(
                        alignment: Alignment.centerLeft,
                        fit: BoxFit.scaleDown,
                        child: Text('Title'),
                      ),
                      subtitle: FittedBox(
                        alignment: Alignment.centerLeft,
                        fit: BoxFit.scaleDown,
                        child: Text('SubTitle'),
                      ),
                      trailing: Icon(
                        Icons.star,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  child: Image.asset(
                    'images/SM_logo.png',
                    fit: BoxFit.cover,
                  )),
              new GridTile(
                  footer: GestureDetector(
                    onTap: () {},
                    child: GridTileBar(
                      backgroundColor: Colors.black45,
                      title: FittedBox(
                        alignment: Alignment.centerLeft,
                        fit: BoxFit.scaleDown,
                        child: Text('Title'),
                      ),
                      subtitle: FittedBox(
                        alignment: Alignment.centerLeft,
                        fit: BoxFit.scaleDown,
                        child: Text('SubTitle'),
                      ),
                      trailing: Icon(
                        Icons.star,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  child: Image.asset(
                    'images/SM_logo.png',
                    fit: BoxFit.cover,
                  )),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildContent2(BuildContext context, Pasien pasien) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => SetResepPage.show(
          context,
          pasien: pasien,
          database: database,
        ),
      ),
      body: Stack(
        children: [
          new Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/background.png'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          new Container(
            color: Colors.black54,
            constraints: BoxConstraints.expand(),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: ListView(
              children: [
                StreamBuilder<List<Resep>>(
                  stream: database.resepStream(pasien: pasien),
                  builder: (context, snapshot) {
                    return ListItemsBuilder<Resep>(
                      snapshot: snapshot,
                      itemBuilder: (context, resep) {
                        return DismisResepList(
                          key: Key('medical-${resep.id}'),
                          pasien: pasien,
                          resep: resep,
                          onDismissed: () => _deleteResep(context, resep),
                          onTap: () => SetResepPage.show2(
                              context: context,
                              database: database,
                              resep: resep,
                              pasien: pasien),
                        );
                      },
                    );
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildContent(BuildContext context, Pasien pasien) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => SetMedicalPage.show(
          context,
          pasien: pasien,
          database: database,
        ),
      ),
      body: Stack(
        children: [
          new Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/doctor.png'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          new Container(
            color: Colors.black54,
            constraints: BoxConstraints.expand(),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: ListView(
              children: [
                StreamBuilder<List<Medical>>(
                  stream: database.medicalStream(pasien: pasien),
                  builder: (context, snapshot) {
                    return ListItemsBuilder<Medical>(
                      snapshot: snapshot,
                      itemBuilder: (context, medical) {
                        return DismissibleMedicalList(
                          key: Key('medical-${medical.id}'),
                          pasien: pasien,
                          medical: medical,
                          onDismissed: () => _deleteMedical(context, medical),
                          onTap: () => SetMedicalPage.show2(
                              context: context,
                              database: database,
                              medical: medical,
                              pasien: pasien),
                          // onTap: () => MedicalPage.show2(
                          //   context: context,
                          //   database: database,
                          //   profile: profile,
                          //   user: user,
                          // ),
                        );
                      },
                    );
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
