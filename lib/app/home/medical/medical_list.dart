import 'package:olohealth/app/home/models/entry.dart';
import 'package:olohealth/app/home/models/profile.dart';
import 'package:olohealth/common_widgets/profile_button.dart';
import 'package:olohealth/services/auth.dart';
import 'package:flutter/material.dart';

class MedicalListItem extends StatelessWidget {
  const MedicalListItem({
    @required this.user,
    this.profile,
    @required this.medical,
    @required this.onTap,
  });

  final User user;
  final Profile profile;
  final Medical medical;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
        child: _buildContents(context),
      ),
    );
  }

  Widget _buildContents(BuildContext context) {
    return Container(
      child: Card(
        child: Stack(
          children: <Widget>[
            Container(
              // constraints: BoxConstraints.expand(),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("images/card_back.png"),
                  fit: BoxFit.cover,
                  alignment: Alignment.topCenter,
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        constraints: BoxConstraints.expand(
                          width: MediaQuery.of(context).size.width / 2,
                          height: MediaQuery.of(context).size.height,
                        ),
                        // child: Image.asset(
                        //   '${profile.avatar}',
                        //   fit: BoxFit.cover,
                        //   height: MediaQuery.of(context).size.width / 2.5,
                        // ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                            colors: <Color>[
                              Colors.blue[300].withAlpha(87),
                              Colors.blue[300].withAlpha(45),
                              Colors.blue[300].withAlpha(12),
                              // Colors.transparent,
                            ],
                          ),
                        ),
                        // constraints: BoxConstraints.expand(),
                        height: MediaQuery.of(context).size.height,
                        child: ListView(
                          // mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            ListTile(
                              title: Text(
                                '${profile.name}',
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 48,
                                  fontWeight: FontWeight.w900,
                                ),
                              ),
                              subtitle: Text(
                                '${profile.gender}| ${profile.age} Tahun',
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 24,
                                  fontWeight: FontWeight.w900,
                                ),
                              ),
                              leading: Image.asset(
                                '${profile.avatar}',
                                fit: BoxFit.cover,
                              ),
                            ),
                            const Divider(
                              color: Colors.grey,
                              height: 20,
                              thickness: 5,
                              indent: 10,
                              endIndent: 10,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    width: 150,
                                    alignment: Alignment.topLeft,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text('Alamat'),
                                        Text('Pekerjaan'),
                                        Text('No Telp'),
                                        Text('No HP'),
                                        Text('Tempat/Tanggal Lahir'),
                                        Text('Alergi'),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(': ${profile.adress}'),
                                        Text(': ${profile.pekerjaan}'),
                                        Text(': ${profile.phone}'),
                                        Text(': ${profile.handphone}'),
                                        Text(
                                            ': ${profile.templahir}, ${profile.ttl}'),
                                        Text(': ${profile.alergi}'),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const Divider(
                              color: Colors.grey,
                              height: 20,
                              thickness: 5,
                              indent: 10,
                              endIndent: 10,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  // Expanded(
                                  //   child: ProfileButton(
                                  //     onPressed: () {},
                                  //     text: 'Data',
                                  //     textColor: Colors.black,
                                  //     color: Colors.blue,
                                  //     onColor: Colors.purple,
                                  //   ),
                                  // ),
                                  // Expanded(
                                  //   child: ProfileButton(
                                  //     onPressed: () {},
                                  //     text: 'Data',
                                  //     textColor: Colors.black,
                                  //     color: Colors.blue,
                                  //     onColor: Colors.purple,
                                  //   ),
                                  // ),
                                  // Expanded(
                                  //   child: ProfileButton(
                                  //     onPressed: () {},
                                  //     text: 'Data',
                                  //     textColor: Colors.black,
                                  //     color: Colors.blue,
                                  //     onColor: Colors.purple,
                                  //   ),
                                  // ),
                                  ProfileButton(
                                    onPressed: () {},
                                    text: 'Profile',
                                    textColor: Colors.black,
                                    color: Colors.blue,
                                    onColor: Colors.purple,
                                  ),
                                  ProfileButton(
                                    onPressed: () {},
                                    text: 'Data',
                                    textColor: Colors.black,
                                    color: Colors.blue,
                                    onColor: Colors.purple,
                                  ),
                                  ProfileButton(
                                    onPressed: () {},
                                    text: 'Data',
                                    textColor: Colors.black,
                                    color: Colors.blue,
                                    onColor: Colors.purple,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              top: 90,
              left: 30,
              child: Image.asset(
                '${profile.avatar}',
                fit: BoxFit.cover,
                height: 300,
              ),
            ),
            Positioned(
              top: 6,
              left: 10,
              child: Column(
                children: <Widget>[
                  Text(
                    'Pasien Profile',
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 36,
                      fontWeight: FontWeight.w900,
                    ),
                  ),
                  const Divider(
                    color: Colors.grey,
                    height: 20,
                    thickness: 5,
                    indent: 10,
                    endIndent: 10,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Widget _buildContents(BuildContext context) {
  //   final dayOfWeek = Format.dayOfWeek(medical.lastCheckUp);
  //   final startDate = Format.date(medical.lastCheckUp);
  //   final startTime = TimeOfDay.fromDateTime(medical.lastCheckUp).format(context);
  //   final endTime = TimeOfDay.fromDateTime(medical.end).format(context);
  //   final durationFormatted = Format.hours(medical.durationInHours);

  //   final pay = profile.age * medical.durationInHours;
  //   final payFormatted = Format.currency(pay);

  //   return Column(
  //     crossAxisAlignment: CrossAxisAlignment.start,
  //     children: <Widget>[
  //       Row(children: <Widget>[
  //         Text(dayOfWeek, style: TextStyle(fontSize: 18.0, color: Colors.grey)),
  //         SizedBox(width: 15.0),
  //         Text(startDate, style: TextStyle(fontSize: 18.0)),
  //         if (profile.age > 0.0) ...<Widget>[
  //           Expanded(child: Container()),
  //           Text(
  //             payFormatted,
  //             style: TextStyle(fontSize: 16.0, color: Colors.green[700]),
  //           ),
  //         ],
  //       ]),
  //       Row(children: <Widget>[
  //         Text('$startTime - $endTime', style: TextStyle(fontSize: 16.0)),
  //         Expanded(child: Container()),
  //         Text(durationFormatted, style: TextStyle(fontSize: 16.0)),
  //       ]),
  //       if (medical.keluhan1.isNotEmpty)
  //         Text(
  //           medical.keluhan1,
  //           style: TextStyle(fontSize: 12.0),
  //           overflow: TextOverflow.ellipsis,
  //           maxLines: 1,
  //         ),
  //     ],
  //   );
  // }
}

class DismissibleMedicalListItem extends StatelessWidget {
  const DismissibleMedicalListItem({
    this.key,
    this.user,
    this.profile,
    this.medical,
    this.onDismissed,
    this.onTap,
  });

  final Key key;
  final User user;
  final Profile profile;
  final Medical medical;
  final VoidCallback onDismissed;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      background: Container(color: Colors.red),
      key: key,
      direction: DismissDirection.endToStart,
      onDismissed: (direction) => onDismissed(),
      child: MedicalListItem(
        user: user,
        medical: medical,
        onTap: onTap,
      ),
    );
  }
}
