import 'dart:async';
import 'package:olohealth/app/home/models/entry.dart';
import 'package:olohealth/app/home/models/pasien.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:olohealth/app/home/models/vitalsign.dart';

import '../../../common_widgets/platform_alert_dialog.dart';
import '../../../common_widgets/platform_exeption_alert_dialog.dart';
import '../../../services/database.dart';
import '../../sign_in/sign_in_button.dart';


class SetMedicalPage extends StatefulWidget {
  const SetMedicalPage(
      {Key key, @required this.database, this.medical, this.pasien})
      : super(key: key);
  final Database database;
  final Medical medical;
  final Pasien pasien;

  static Future<void> show(BuildContext context,
      {Database database, Pasien pasien}) async {
    await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => SetMedicalPage(
          database: database,
          pasien: pasien,
        ),
        fullscreenDialog: false,
      ),
    );
  }

  static Future<void> show2(
      {BuildContext context,
      Database database,
      Medical medical,
      Pasien pasien}) async {
    await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => SetMedicalPage(
            database: database, medical: medical, pasien: pasien),
        fullscreenDialog: true,
      ),
    );
  }

  @override
  _SetMedicalPageState createState() => _SetMedicalPageState();
}

class _SetMedicalPageState extends State<SetMedicalPage> {
  final _formKey = GlobalKey<FormState>();
  // DateTime _end;
  String _keluhan1;
  String _keluhan2;
  String _inspeksi;
  String _palpasi;
  String _perkusi;
  String _auskultasi;
  String _diagnose1;
  String _diagnose2;
  String _inejksi;
  String _terapi;
  // String _resep;

  @override
  void initState() {
    super.initState();
    print('may pasienId: ' + widget.pasien.id);
    if (widget.medical != null) {
      // _end = widget.medical.end;
      // _berat = widget.medical.berat;
      _keluhan1 = widget.medical.keluhan1;
      _keluhan2 = widget.medical.keluhan2;
      // _tekanandarah = widget.medical.tekanandarah;
      // _nadi = widget.medical.nadi;
      _inspeksi = widget.medical.inspeksi;
      _palpasi = widget.medical.palpasi;
      _perkusi = widget.medical.perkusi;
      _auskultasi = widget.medical.auskultasi;
      _diagnose1 = widget.medical.diagnose1;
      _diagnose2 = widget.medical.diagnose2;
      _inejksi = widget.medical.injeksi;
      _terapi = widget.medical.terapi;
      // _resep = widget.medical.resep;
    }
  }

  bool _saveForm() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Future<void> _submit() async {
    print('My pasienID: ${widget.pasien?.id}');
    if (_saveForm()) {
      try {
        final medicals = await widget.database.medicalStream2().first;
        final allNames = medicals.map((e) => e.keluhan1).toList();
        if (widget.medical != null) {
          allNames.remove(widget.medical.keluhan1);
        }
        if (allNames.contains(_keluhan1)) {
          PlatformAlertDialog(
            title: 'name already used',
            content: 'please use a diffrent medical names',
            defaultActionText: 'OK',
          ).show(context);
        } else {
          final id = widget.medical?.id ?? documentIdFromCurrentDate();
          final lastDate = widget.medical?.lastCheckUp ?? DateTime.now();
          final uid = widget.pasien.id;
          final medical = Medical(
            id: id,
            uid: uid,
            lastCheckUp: lastDate,
            // end: _end,
            keluhan1: _keluhan1,
            keluhan2: _keluhan2,
            // berat: _berat,
            // tekanandarah: _tekanandarah,
            // nadi: _nadi,
            inspeksi: _inspeksi,
            palpasi: _palpasi,
            perkusi: _perkusi,
            auskultasi: _auskultasi,
            diagnose1: _diagnose1,
            diagnose2: _diagnose2,
            injeksi: _inejksi,
            terapi: _terapi,
            // resep: _resep,
          );
          await widget.database.setMedical2(medical);
          Navigator.of(context).pop();
        }
      } on PlatformException catch (e) {
        PlatformExeptionAlertDialog(
          title: 'operation failed',
          exception: e,
        ).show(context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    print('form id : ' + widget.pasien.vitalId);
    return StreamBuilder<Vital>(
      stream: widget.database.readVital(userId: widget.pasien.vitalId),
      builder: (context, snapshot) {
        final vital = snapshot.data;
        final bmi = vital?.bmi;
        final height = vital?.tinggi;
        final weight = vital?.berat;
        if (snapshot.hasData) {
          return Scaffold(
            backgroundColor: Colors.black54,
            appBar: PreferredSize(
              preferredSize:
                  Size.fromHeight(MediaQuery.of(context).size.height * 0.27),
              child: AppBar(
                backgroundColor: Colors.black54,
                elevation: 2.0,
                flexibleSpace: FlexibleSpaceBar(
                  centerTitle: false,
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(width: 50),
                      Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: Container(
                          width: 80,
                          height: 80,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(40)),
                            image: DecorationImage(
                              image: AssetImage(widget.pasien.avatar),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                      // SizedBox(width: 20),
                      // Padding(
                      //   padding: const EdgeInsets.only(top: 5),
                      //   child: IconButton(
                      //     color: Colors.blue,
                      //     iconSize: 35,
                      //     icon: Icon(Icons.videocam),
                      //     onPressed: _launchURL,
                      //     // onPressed: _launchHtml,
                      //   ),
                      // ),
                      SizedBox(width: 20),
                      Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 20),
                            Row(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text('Name'),
                                    Text('Age'),
                                    Text('Gender'),
                                  ],
                                ),
                                SizedBox(width: 20),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(': ' + widget.pasien.name),
                                    Text(': ' +
                                        widget.pasien.age.toStringAsFixed(0)),
                                    Text(': ' + widget.pasien.gender),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(width: 50),
                      Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 20),
                            Row(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text('BMI'),
                                    Text('Height'),
                                    Text('Weight'),
                                    Text('Tekanan Darah'),
                                  ],
                                ),
                                SizedBox(width: 20),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(': ${bmi.toStringAsFixed(2)}'),
                                    Text(': ${height.toStringAsFixed(0)} cm'),
                                    Text(': ${weight.toString()} Kg'),
                                    Text(': ${vital.tekanandarah}'),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(width: 50),
                      Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 20),
                            Row(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text('Nadi'),
                                    Text('Respiration Rate'),
                                    Text('SPO2'),
                                    Text('Temperature'),
                                  ],
                                ),
                                SizedBox(width: 20),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(': ${vital.nadi.toString()}'),
                                    Text(
                                        ': ${vital.respiration.toStringAsFixed(2)}'),
                                    Text(': ${vital.spo2.toStringAsFixed(2)}'),
                                    Text(': ${vital.temp.toStringAsFixed(2)}'),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            body: Stack(
              children: <Widget>[
                new Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('images/suster.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                new Container(
                  child: _buildContent(),
                ),
              ],
            ),
            // body: _buildContent(),
          );
        }
        return Scaffold(
          body: Center(child: CircularProgressIndicator()),
        );
      },
    );
  }

  Widget _buildContent() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: _buildForm(),
          ),
        ),
      ),
    );
  }

  _buildForm() {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: _buildFormChildren(),
      ),
    );
  }

  _buildFormChildren() {
    return [
      TextFormField(
        decoration: InputDecoration(labelText: 'Keluhan Utama'),
        initialValue: _keluhan1,
        validator: (value) =>
            value.isNotEmpty ? null : 'Keluhan Utama tidak boleh kosong',
        onSaved: (value) => _keluhan1 = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Keluhan Lain'),
        initialValue: _keluhan2,
        // validator: (value) => value.isNotEmpty ? null : 'Keluhan Utama tidak boleh kosong',
        onSaved: (value) => _keluhan2 = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Inspeksi'),
        initialValue: _inspeksi,
        onSaved: (value) => _inspeksi = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Palpasi'),
        initialValue: _palpasi,
        onSaved: (value) => _palpasi = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Perkusi'),
        initialValue: _perkusi,
        onSaved: (value) => _perkusi = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Auskultasi'),
        initialValue: _auskultasi,
        onSaved: (value) => _auskultasi = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Working Diagnose'),
        initialValue: _diagnose1,
        onSaved: (value) => _diagnose1 = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Differential Diagnose'),
        initialValue: _diagnose2,
        onSaved: (value) => _diagnose2 = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Injeksi'),
        initialValue: _inejksi,
        onSaved: (value) => _inejksi = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Terapi/anjuran/tindakan'),
        initialValue: _terapi,
        onSaved: (value) => _terapi = value,
      ),
      SizedBox(height: 10.0),
      SignInButton(
        text: 'Done',
        textColor: Colors.white,
        color: Colors.indigo[500],
        onPressed: _submit,
        height: 50,
        borderRadius: 10,
      ),
    ];
  }
}
