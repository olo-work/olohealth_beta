import 'package:olohealth/app/home/medical/date_time_picker.dart';
import 'package:olohealth/app/home/models/entry.dart';
import 'package:olohealth/app/home/models/profile.dart';
import 'package:olohealth/common_widgets/platform_exeption_alert_dialog.dart';
import 'package:olohealth/services/auth.dart';
import 'package:olohealth/services/database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MedicalPage extends StatefulWidget {
  const MedicalPage({@required this.database, @required this.profile, this.medical, this.user});
  final Profile profile;
  final Medical medical;
  final Database database;
  final User user;

  static Future<void> show({BuildContext context, Database database, Profile profile, Medical medical}) async {
    await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) =>
            MedicalPage(database: database, profile: profile, medical: medical),
        fullscreenDialog: true,
      ),
    );
  }

  static Future<void> show2({BuildContext context, Database database, Profile profile, User user}) async {
    await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) =>
            MedicalPage(database: database, profile: profile, user: user),
        fullscreenDialog: true,
      ),
    );
  }

  @override
  State<StatefulWidget> createState() => _MedicalPageState();
}

class _MedicalPageState extends State<MedicalPage> {
  DateTime _startDate;
  TimeOfDay _startTime;
  DateTime _endDate;
  TimeOfDay _endTime;
  String _comment;

  @override
  void initState() {
    super.initState();
    // final start = widget.medical?.lastCheckUp ?? DateTime.now();
    // _startDate = DateTime(start.year, start.month, start.day);
    // _startTime = TimeOfDay.fromDateTime(start);

    // final end = widget.medical?.end ?? DateTime.now();
    // _endDate = DateTime(end.year, end.month, end.day);
    // _endTime = TimeOfDay.fromDateTime(end);

    _comment = widget.medical?.keluhan1 ?? '';
  }

  Medical _medicalFromState() {
    // final start = DateTime(_startDate.year, _startDate.month, _startDate.day,
    //     _startTime.hour, _startTime.minute);
    // final end = DateTime(_endDate.year, _endDate.month, _endDate.day,
    //     _endTime.hour, _endTime.minute);
    final id = widget.medical?.id ?? documentIdFromCurrentDate();
    return Medical(
      id: id,
      uid: widget.profile.id,
      // lastCheckUp: start,
      // end: end,
      keluhan1: _comment,
    );
  }

  Future<void> _setMedicalAndDismiss(BuildContext context) async {
    try {
      final medical = _medicalFromState();
      await widget.database.setMedical(medical);
      Navigator.of(context).pop();
    } on PlatformException catch (e) {
      PlatformExeptionAlertDialog(
        title: 'Operation failed',
        exception: e,
      ).show(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 2.0,
        title: Text(widget.profile.name),
        actions: <Widget>[
          FlatButton(
            child: Text(
              widget.medical != null ? 'Update' : 'Create',
              style: TextStyle(fontSize: 18.0, color: Colors.white),
            ),
            onPressed: () => _setMedicalAndDismiss(context),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _buildStartDate(),
              _buildEndDate(),
              SizedBox(height: 8.0),
              _buildDuration(),
              SizedBox(height: 8.0),
              _buildComment(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildStartDate() {
    return DateTimePicker(
      labelText: 'Start',
      selectedDate: _startDate,
      selectedTime: _startTime,
      selectDate: (date) => setState(() => _startDate = date),
      selectTime: (time) => setState(() => _startTime = time),
    );
  }

  Widget _buildEndDate() {
    return DateTimePicker(
      labelText: 'End',
      selectedDate: _endDate,
      selectedTime: _endTime,
      selectDate: (date) => setState(() => _endDate = date),
      selectTime: (time) => setState(() => _endTime = time),
    );
  }

  Widget _buildDuration() {
    //final currentMedical = _medicalFromState();
    //final durationFormatted = Format.hours(currentMedical);
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Text(
          'test',
          // 'Duration: $durationFormatted',
          style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w500),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
      ],
    );
  }

  Widget _buildComment() {
    return TextField(
      keyboardType: TextInputType.text,
      maxLength: 50,
      controller: TextEditingController(text: _comment),
      decoration: InputDecoration(
        labelText: 'Comment',
        labelStyle: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w500),
      ),
      style: TextStyle(fontSize: 20.0, color: Colors.black),
      maxLines: null,
      onChanged: (comment) => _comment = comment,
    );
  }
}
