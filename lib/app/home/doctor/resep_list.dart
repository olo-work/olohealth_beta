import 'package:olohealth/app/home/medical/format.dart';
import 'package:olohealth/app/home/models/pasien.dart';
import 'package:flutter/material.dart';
import 'package:olohealth/app/home/models/resep.dart';

class ResepListItem extends StatelessWidget {
  const ResepListItem({
    @required this.pasien,
    @required this.resep,
    @required this.onTap,
  });

  final Pasien pasien;
  final Resep resep;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    // DateTime rekamDate = DateTime.parse(medical.id);
    return Container(
        height: MediaQuery.of(context).size.height * 0.4,
        child: InkWell(
          onTap: onTap,
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
            child: _buildContents(context),
          ),
        ),
      );
    // return Container(
    //   // constraints: BoxConstraints.expand(
    //   //   height: MediaQuery.of(context).size.height * 0.8,
    //   //   width: MediaQuery.of(context).size.width,
    //   // ),
    //   child: Stack(
    //     children: [
    //       new Container(
    //         decoration: BoxDecoration(
    //           image: DecorationImage(
    //             image: AssetImage('images/doctor.png'),
    //             fit: BoxFit.cover,
    //           ),
    //         ),
    //       ),
    //       new Container(
    //         color: Colors.black54,
    //         // constraints: BoxConstraints.expand(
    //         //   // height: MediaQuery.of(context).size.height,
    //         //   // width: MediaQuery.of(context).size.width,
    //         // ),
    //       ),
    //       InkWell(
    //         onTap: onTap,
    //         child: Container(
    //           constraints: BoxConstraints.expand(),
    //           margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
    //           child: _buildContents(context),
    //         ),
    //       ),
    //     ],
    //   ),
    // );
  }

  Widget _buildContents(BuildContext context) {
    final lastDate = Format.date(resep.resepDate);
    return Card(
      child: Stack(
        children: [
          Container(
            color: Colors.black.withOpacity(0.7),
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  // SizedBox(width: 30),
                  Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Resep Obat 1', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text('Resep Obat 2', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text('Resep Obat 3', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text('Resep Obat 4', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text('Resep Obat 5', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text('Resep Obat 6', style: TextStyle(fontSize: 20, color: Colors.white)),
                      ],
                    ),
                  ),
                  SizedBox(width: 30),
                  Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(': ${resep.obat1}', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text(': ${resep.obat2}', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text(': ${resep.obat3}', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text(': ${resep.obat4}', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text(': ${resep.obat5}', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text(': ${resep.obat6}', style: TextStyle(fontSize: 20, color: Colors.white)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: 6,
            left: 10,
            child: Column(
              children: <Widget>[
                Text(
                  'Resep - ' + lastDate,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 36,
                    fontWeight: FontWeight.w900,
                  ),
                ),
                const Divider(
                  color: Colors.grey,
                  height: 20,
                  thickness: 5,
                  indent: 10,
                  endIndent: 10,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class DismisResepList extends StatelessWidget {
  const DismisResepList({
    this.key,
    this.pasien,
    this.resep,
    this.onDismissed,
    this.onTap,
  });

  final Key key;
  final Pasien pasien;
  final Resep resep;
  final VoidCallback onDismissed;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      background: Container(color: Colors.red),
      key: key,
      direction: DismissDirection.endToStart,
      onDismissed: (direction) => onDismissed(),
      child: ResepListItem(
        pasien: pasien,
        resep: resep,
        onTap: onTap,
      ),
    );
  }
}
