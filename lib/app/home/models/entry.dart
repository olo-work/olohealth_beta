import 'package:flutter/foundation.dart';

class Medical {
  Medical({
    @required this.id,
    // @required this.profileId,
    this.lastCheckUp,
    // @required this.end,
    this.keluhan1,
    this.keluhan2,
    this.berat,
    this.tekanandarah,
    this.nadi,
    this.inspeksi,
    this.palpasi,
    this.perkusi,
    this.auskultasi,
    this.diagnose1,
    this.diagnose2,
    this.injeksi,
    this.terapi,
    this.resep,
    this.uid,
  });

  String id;
  // String profileId;
  DateTime lastCheckUp;
  // DateTime end;
  String keluhan1;
  String keluhan2;
  double berat;
  String tekanandarah;
  String nadi;
  String inspeksi;
  String palpasi;
  String perkusi;
  String auskultasi;
  String diagnose1;
  String diagnose2;
  String injeksi;
  String terapi;
  String resep;
  String uid;

  // double get durationInHours =>
  //     end.difference(lastCheckUp).inMinutes.toDouble() / 60.0;

  factory Medical.fromMap(Map<dynamic, dynamic> value, String documentId) {
    if (value == null) {
      return null;
    }
    final int lcuMilliseconds = value['lastCheckUp'];
    // final int endMilliseconds = value['end'];
    return Medical(
      id: documentId,
      uid: value['uid'],
      // profileId: value['profileId'],
      lastCheckUp: DateTime.fromMillisecondsSinceEpoch(lcuMilliseconds),
      // end: DateTime.fromMillisecondsSinceEpoch(endMilliseconds),
      keluhan1: value['keluhan1'],
      keluhan2: value['keluhan2'],
      berat: value['berat'],
      tekanandarah: value['tekanandarah'],
      nadi: value['nadi'],
      inspeksi: value['inspeksi'],
      palpasi: value['palpasi'],
      perkusi: value['perkusi'],
      auskultasi: value['auskultasi'],
      diagnose1: value['diagnose1'],
      diagnose2: value['diagnose2'],
      injeksi: value['injeksi'],
      terapi: value['terapi'],
      resep: value['resep'],
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      // 'profileId': profileId,
      'lastCheckUp': lastCheckUp.millisecondsSinceEpoch,
      // 'end': end.millisecondsSinceEpoch,
      'uid': uid,
      'keluhan1': keluhan1,
      'keluhan2': keluhan2,
      'berat': berat,
      'tekanandarah': tekanandarah,
      'nadi': nadi,
      'inspeksi': inspeksi,
      'palpasi': palpasi,
      'perkusi': perkusi,
      'auskultasi': auskultasi,
      'diagnose1': diagnose1,
      'diagnose2': diagnose2,
      'injeksi': injeksi,
      'terapi': terapi,
      'resep': resep,

    };
  }
}
