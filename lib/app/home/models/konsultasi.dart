class Konsultasi {
  Konsultasi({
    this.id,
    this.pasienId,
    this.resepId,
    this.doctorId,
    this.medicalId,
    this.tanggal,
    this.pasienName,
    this.pasienAge,
    this.pasienGender,
    this.doctorName,
  });
  final String id;
  final String pasienId;
  final String resepId;
  final String doctorId;
  final String medicalId;
  final DateTime tanggal;
  final String pasienName;
  final double pasienAge;
  final String pasienGender;
  final String doctorName;

  factory Konsultasi.fromMap(Map<String, dynamic> data, String documentId) {
    if (data == null) {
      return null;
    }
    final String pasienId = data['pasienId'];
    final String resepId = data['pasienId'];
    final String doctorId = data['pasienId'];
    final String medicalId = data['pasienId'];
    final int tanggalMili = data['tanggal'];
    final String pasienName = data['pasienName'];
    final String pasienGender = data['pasienGender'];
    final String doctorName = data['doctorName'];
    final double pasienAge = data['pasienAge'];
    return Konsultasi(
      id: documentId,
      pasienId: pasienId,
      pasienName: pasienName,
      pasienGender: pasienGender,
      pasienAge: pasienAge,
      doctorId: doctorId,
      doctorName: doctorName,
      resepId: resepId,
      medicalId: medicalId,
      tanggal: DateTime.fromMillisecondsSinceEpoch(tanggalMili),
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'pasienId': pasienId,
      'pasienName': pasienName,
      'pasienGender': pasienGender,
      'pasienAge': pasienAge,
      'doctorId': doctorId,
      'doctorName': doctorName,
      'resepId': resepId,
      'medicalId': medicalId,
      'tanggal': tanggal.millisecondsSinceEpoch,
    };
  }
}
