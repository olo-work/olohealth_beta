import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'Platform_widget.dart';

class PlatformAlertDialog extends PlatformWidget {
  PlatformAlertDialog({
    @required this.title,
    @required this.content,
    this.cancelAction,
    @required this.defaultActionText,
  })  : assert(title != null),
        assert(content != null),
        assert(defaultActionText != null);

  final String title;
  final String content;
  final String cancelAction;
  final String defaultActionText;

  Future<bool> show(BuildContext context) async {
    return await showDialog<bool>(
      context: context,
      barrierDismissible: false,
      builder: (context) => this,
    );
  }

  // Future<bool> show(BuildContext context) async {
  //   return kIsWeb == false
  //       ? await showCupertinoDialog<bool>(
  //           context: context,
  //           builder: (context) => this,
  //         )
  //       : await showDialog<bool>(
  //           context: context,
  //           barrierDismissible: false,
  //           builder: (context) => this,
  //         );
  // }

  @override
  Widget buildAndroidAlert(BuildContext context) {
    return AlertDialog(
      title: Text(title),
      content: Text(content),
      actions: _buildAction(context),
    );
  }

  // @override
  // Widget buildIOSAlert(BuildContext context) {
  //   return CupertinoAlertDialog(
  //     title: Text(title),
  //     content: Text(content),
  //     actions: _buildAction(context),
  //   );
  // }

  List<Widget> _buildAction(BuildContext context) {
    final action = <Widget>[];
    if (cancelAction != null) {
      action.add(
        PlatformAlertDialogAction(
          onPressed: () => Navigator.of(context).pop(false),
          child: Text(cancelAction),
        ),
      );
    }
    action.add(
      PlatformAlertDialogAction(
        onPressed: () => Navigator.of(context).pop(true),
        child: Text(defaultActionText),
      ),
    );
    return action;
  }
}

class PlatformAlertDialogAction extends PlatformWidget {
  PlatformAlertDialogAction({this.child, this.onPressed});
  final Widget child;
  final VoidCallback onPressed;

  @override
  Widget buildAndroidAlert(BuildContext context) {
    return FlatButton(
      onPressed: onPressed,
      child: child,
    );
  }

  // @override
  // Widget buildIOSAlert(BuildContext context) {
  //   return CupertinoDialogAction(
  //     child: child,
  //     onPressed: onPressed,
  //   );
  // }
}
