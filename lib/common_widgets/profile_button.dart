import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'costum_outline_button.dart';


class ProfileButton extends CustomOutlineButton {
  ProfileButton({
    @required String text,
    Color color,
    Color onColor,
    Color textColor,
    VoidCallback onPressed,
  }) : super(
    child: Text(
      text,
      style: TextStyle(
        color: textColor,
        fontSize: 12.0,
      ),
    ),
    height: 80,
    color: color,
    onColor: onColor,
    onPressed: onPressed,
  );
}